#include <iostream>
#include <exception>
#include <db_cxx.h>
#include <BerkelyDB/Environment.h>
#include <BerkelyDB/Database.h>

int main()
{
	// the back-end might leak some exceptions we will want to catch..
	try
	{
		BerkelyDB::Environment environment("/home/ronald/.versie");
std::clog << "ping!" << std::endl;
		BerkelyDB::Database handle(&environment, "versie.db");
	}
	catch (const DbException & e)
	{
		std::cerr << e.what() << std::endl;
	}
	catch (const std::exception & e)
	{
		std::cerr << e.what() << std::endl;
	}
	catch (...)
	{
		std::cerr << "Unhandled exception of unknown type" << std::endl;
	}

	return 0;
}

