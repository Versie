#ifndef _versie_tests_keydatapair_h
#define _versie_tests_keydatapair_h

#include <cppunit/extensions/HelperMacros.h>

namespace Tests
{
	class KeyDataPair : public CPPUNIT_NS::TestFixture
	{
		CPPUNIT_TEST_SUITE(KeyDataPair);
		CPPUNIT_TEST(testIntegerAsKey);
		CPPUNIT_TEST(testIntegerAsData);
		CPPUNIT_TEST(tryGetWrongSizeKey);
		CPPUNIT_TEST(tryGetWrongSizeData);
		CPPUNIT_TEST(copyConstructAndCompare);
		CPPUNIT_TEST_SUITE_END();
	public :

		void setUp();
		void tearDown();

	protected :
		void testIntegerAsKey();
		void testIntegerAsData();
		void tryGetWrongSizeKey();
		void tryGetWrongSizeData();
		void copyConstructAndCompare();

	private :
	};
}

#endif

