#include "Transactions.h"
#include "BerkelyDB.h" // for BerkelyDB::createTempName
#include <BerkelyDB/Environment.h>
#include <BerkelyDB/Database.h>
#include <BerkelyDB/KeyDataPair.h>
#include <BerkelyDB/Transaction.h>
#include <BerkelyDB/Exceptions/Deadlock.h>
#ifdef HAVE_BOOST_FILESYSTEM
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/convenience.hpp>
#endif

namespace Tests
{
	CPPUNIT_TEST_SUITE_REGISTRATION(Transactions);

	void Transactions::setUp()
	{ /* no-op */ }

	void Transactions::tearDown()
	{ /* no-op */ }

	namespace
	{
		enum {
			test_environment_flags = ::BerkelyDB::Environment::create | ::BerkelyDB::Environment::transactions | ::BerkelyDB::Environment::recovery,
			test_database_flags = ::BerkelyDB::Database::create | ::BerkelyDB::Database::auto_commit,
		};
	}
	
	void Transactions::createEnvironment()
	{
		std::string env_name(BerkelyDB::createTempName());
		env_name = "test/" + env_name;
		boost::filesystem::create_directories(boost::filesystem::path(env_name, boost::filesystem::native));
		::BerkelyDB::Environment env(env_name, test_environment_flags);
	}

	void Transactions::createDatabase()
	{
		std::string env_name(BerkelyDB::createTempName());
		env_name = "test/" + env_name;
		boost::filesystem::create_directories(boost::filesystem::path(env_name, boost::filesystem::native));
		::BerkelyDB::Environment env(env_name, test_environment_flags);
		::BerkelyDB::Database db(&env, BerkelyDB::createTempName(), test_database_flags);
	}

	void Transactions::tryAbort()
	{
		std::string env_name(BerkelyDB::createTempName());
		env_name = "test/" + env_name;
		boost::filesystem::create_directories(boost::filesystem::path(env_name, boost::filesystem::native));
		::BerkelyDB::Environment env(env_name, test_environment_flags);
		::BerkelyDB::Database db(&env, BerkelyDB::createTempName(), test_database_flags);

		::BerkelyDB::Transaction transaction(&env);
		db.insert(::BerkelyDB::KeyDataPair("hello", "transaction"), false, transaction);
		transaction.abort();
		
		CPPUNIT_ASSERT(db.empty());
	}

	void Transactions::tryCommit()
	{
		std::string env_name(BerkelyDB::createTempName());
		env_name = "test/" + env_name;
		boost::filesystem::create_directories(boost::filesystem::path(env_name, boost::filesystem::native));
		::BerkelyDB::Environment env(env_name, test_environment_flags);
		::BerkelyDB::Database db(&env, BerkelyDB::createTempName(), test_database_flags);

		::BerkelyDB::Transaction transaction(&env);
		db.insert(::BerkelyDB::KeyDataPair("hello", "transaction"), false, transaction);
		transaction.commit();
		
		CPPUNIT_ASSERT(::BerkelyDB::KeyDataPair(*db.begin()) == ::BerkelyDB::KeyDataPair("hello", "transaction"));
	}

	void Transactions::testCursors()
	{
	}

	void Transactions::tryInsertDeadlock()
	{
		std::string env_name(BerkelyDB::createTempName());
		env_name = "test/" + env_name;
		boost::filesystem::create_directories(boost::filesystem::path(env_name, boost::filesystem::native));
		::BerkelyDB::Environment env(env_name, test_environment_flags, ::BerkelyDB::Environment::abort_random);
		::BerkelyDB::Database db(&env, BerkelyDB::createTempName(), test_database_flags);

		::BerkelyDB::Transaction transaction1(&env, ::BerkelyDB::Transaction::no_wait);
		db.insert(::BerkelyDB::KeyDataPair("hello", "transaction"), false, transaction1);

		bool caught_deadlock = false;
		try
		{
			::BerkelyDB::Transaction transaction2(&env, ::BerkelyDB::Transaction::no_wait);
			db.insert(::BerkelyDB::KeyDataPair("hello", "transaction"), false, transaction2);
		}
		catch (const ::BerkelyDB::Exceptions::Deadlock &)
		{
			caught_deadlock = true;
		}
		CPPUNIT_ASSERT(caught_deadlock);
	}

	void Transactions::tryEraseDeadlock()
	{
		std::string env_name(BerkelyDB::createTempName());
		env_name = "test/" + env_name;
		boost::filesystem::create_directories(boost::filesystem::path(env_name, boost::filesystem::native));
		::BerkelyDB::Environment env(env_name, test_environment_flags, ::BerkelyDB::Environment::abort_random);
		::BerkelyDB::Database db(&env, BerkelyDB::createTempName(), test_database_flags);

		db.insert(::BerkelyDB::KeyDataPair("hello", "transaction"));

		::BerkelyDB::Transaction transaction1(&env, ::BerkelyDB::Transaction::no_wait);
		db.erase("hello", transaction1);

		bool caught_deadlock = false;
		try
		{
			::BerkelyDB::Transaction transaction2(&env, ::BerkelyDB::Transaction::no_wait);
			db.erase("hello", transaction2);
			transaction2.commit();
		}
		catch (const ::BerkelyDB::Exceptions::Deadlock &)
		{
			caught_deadlock = true;
		}
		CPPUNIT_ASSERT(caught_deadlock);
		transaction1.commit();
	}
}

