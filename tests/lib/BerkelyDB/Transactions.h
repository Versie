#ifndef _versie_tests_transaction_h
#define _versie_tests_transaction_h

#include <cppunit/extensions/HelperMacros.h>

namespace Tests
{
	class Transactions : public CPPUNIT_NS::TestFixture
	{
		CPPUNIT_TEST_SUITE(Transactions);
		CPPUNIT_TEST(createEnvironment);
		CPPUNIT_TEST(createDatabase);
		CPPUNIT_TEST(tryAbort);
		CPPUNIT_TEST(tryCommit);
		CPPUNIT_TEST(testCursors);
		CPPUNIT_TEST(tryInsertDeadlock);
		CPPUNIT_TEST(tryEraseDeadlock);
		CPPUNIT_TEST_SUITE_END();
	public :

		void setUp();
		void tearDown();

	protected :
		void createEnvironment();
		void createDatabase();
		void tryAbort();
		void tryCommit();
		void testCursors();
		void tryInsertDeadlock();
		void tryEraseDeadlock();

	private :
	};
}

#endif

