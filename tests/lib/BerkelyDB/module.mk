tests_BerkelyDB_SRC :=		main.cpp		\
				BerkelyDB.cpp		\
				KeyDataPair.cpp
SRC += $(patsubst %,tests/lib/BerkelyDB/%,$(tests_BerkelyDB_SRC))

tests_BerkelyDB_OBJ := $(patsubst %.cpp,tests/lib/BerkelyDB/%.lo,$(tests_BerkelyDB_SRC))
OBJ += $(tests_BerkelyDB_OBJ)

TEST_LDFLAGS=-lcppunit -ldl libBerkelyDB.la -ldb_cxx $(LIBS)
$(eval $(call LINK_BINARY_template,tests_BerkelyDB.bin,$(tests_BerkelyDB_OBJ),$(TEST_LDFLAGS)))

CHECK_DEPS += tests_BerkelyDB.bin

