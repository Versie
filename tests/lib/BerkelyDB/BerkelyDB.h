#ifndef _versie_tests_berkelydb_h
#define _versie_tests_berkelydb_h

#include <cppunit/extensions/HelperMacros.h>

namespace Tests
{
	class BerkelyDB : public CPPUNIT_NS::TestFixture
	{
		CPPUNIT_TEST_SUITE(BerkelyDB);
#ifdef HAVE_BOOST_FILESYSTEM
		CPPUNIT_TEST(createEnvironment);
		CPPUNIT_TEST(createDatabaseInEnvironment);
#endif
		CPPUNIT_TEST(createDatabaseWithoutEnvironment);
#ifdef HAVE_BOOST_FILESYSTEM
		CPPUNIT_TEST(checkEmptyDatabaseIterators);
		CPPUNIT_TEST(checkEmptyDatabaseAccessors);
		CPPUNIT_TEST(createDatabaseAndReopen);
		CPPUNIT_TEST(addValueToDatabase);
		CPPUNIT_TEST(addValueAndReadBackThroughCursor);
		CPPUNIT_TEST(addValueAndReadBackThroughCursor2);
		CPPUNIT_TEST(addManyValuesAndReadBackThroughCursor);
		CPPUNIT_TEST(writeThroughCursor);
		CPPUNIT_TEST(writeThroughCursor2);
		CPPUNIT_TEST(writeThroughCursor3);
		CPPUNIT_TEST(writeThroughCursor4);
		CPPUNIT_TEST(erase);
		CPPUNIT_TEST(clear);
		CPPUNIT_TEST(get);
#endif
		CPPUNIT_TEST_SUITE_END();
	public :
		// Used by other unit tests
		static std::string createTempName();

		void setUp();
		void tearDown();

	protected :
#ifdef HAVE_BOOST_FILESYSTEM
		void createEnvironment();
		void createDatabaseInEnvironment();
#endif
		void createDatabaseWithoutEnvironment();
#ifdef HAVE_BOOST_FILESYSTEM
		void checkEmptyDatabaseIterators();
		void checkEmptyDatabaseAccessors();
		void createDatabaseAndReopen();
		void addValueToDatabase();
		void addValueAndReadBackThroughCursor();
		void addValueAndReadBackThroughCursor2();
		void addManyValuesAndReadBackThroughCursor();
		void writeThroughCursor();
		void writeThroughCursor2();
		void writeThroughCursor3();
		void writeThroughCursor4();
		void erase();
		void clear();
		void get();
#endif
	private :
	};
}

#endif

