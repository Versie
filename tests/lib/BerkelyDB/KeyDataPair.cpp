#include "KeyDataPair.h"
#include <BerkelyDB/KeyDataPair.h>
#include <BerkelyDB/Exceptions/KeyDataPairError.h>

#ifndef CPPUNIT_ASSERT_THROW_MESSAGE
#define CPPUNIT_ASSERT_THROW_MESSAGE(m, a, e) CPPUNIT_ASSERT_THROW(a, e)
#endif

namespace Tests
{
	CPPUNIT_TEST_SUITE_REGISTRATION(KeyDataPair);
	namespace 
	{
		template <typename Integer>
		void testIntegerKey()
		{
			::BerkelyDB::KeyDataPair kdp(Integer(28), 0);
			CPPUNIT_ASSERT(kdp.getKeySize() == sizeof(Integer));
		}

		template <typename Integer>
		void testIntegerData()
		{
			::BerkelyDB::KeyDataPair kdp(0, Integer(28));
			CPPUNIT_ASSERT(kdp.getDataSize() == sizeof(Integer));
		}
	}

	void KeyDataPair::setUp()
	{ /* no-op */ }

	void KeyDataPair::tearDown()
	{ /* no-op */ }

	void KeyDataPair::testIntegerAsKey()
	{
		testIntegerKey<unsigned char>();
		testIntegerKey<signed char>();
		testIntegerKey<unsigned short>();
		testIntegerKey<signed short>();
		testIntegerKey<unsigned long>();
		testIntegerKey<signed long>();
	}

	void KeyDataPair::testIntegerAsData()
	{
		testIntegerData<unsigned char>();
		testIntegerData<signed char>();
		testIntegerData<unsigned short>();
		testIntegerData<signed short>();
		testIntegerData<unsigned long>();
		testIntegerData<signed long>();
	}

	void KeyDataPair::tryGetWrongSizeKey()
	{
		unsigned long ul(28);
		::BerkelyDB::KeyDataPair kdp(ul, 0);
		CPPUNIT_ASSERT_THROW_MESSAGE("Storing an unsigned long and retrieving an unsigned should should throw an exception",
				kdp.getKey<unsigned short>(),  ::BerkelyDB::Exceptions::KeyDataPairError);
	}

	void KeyDataPair::tryGetWrongSizeData()
	{
		unsigned long ul(324);
		unsigned long ul2(2198734);
		::BerkelyDB::KeyDataPair kdp(ul, ul2);
		CPPUNIT_ASSERT_THROW_MESSAGE("Storing an unsigned long and retrieving an unsigned short should should throw an exception",
			kdp.getData<unsigned short>(),  ::BerkelyDB::Exceptions::KeyDataPairError);
	}

	void KeyDataPair::copyConstructAndCompare()
	{
		unsigned long ul(324);
		unsigned long ul2(2198734);
		::BerkelyDB::KeyDataPair kdp1(ul, ul2);
		::BerkelyDB::KeyDataPair kdp2(kdp1);
		CPPUNIT_ASSERT_MESSAGE("After copy construction, the two instances should compare equal", kdp1 == kdp2);
	}
}

