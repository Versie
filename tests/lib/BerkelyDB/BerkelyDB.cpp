#include "BerkelyDB.h"
#include <BerkelyDB/Environment.h>
#include <BerkelyDB/KeyDataPair.h>
#include <BerkelyDB/Exceptions/CursorError.h>
#ifdef HAVE_BOOST_FILESYSTEM
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/convenience.hpp>
#endif
#include <algorithm>

namespace Tests
{
	CPPUNIT_TEST_SUITE_REGISTRATION(BerkelyDB);

	void BerkelyDB::setUp()
	{ /* no-op */ }

	void BerkelyDB::tearDown()
	{ /* no-op */ }

#ifdef HAVE_BOOST_FILESYSTEM
	void BerkelyDB::createEnvironment()
	{
		std::string env_name(createTempName());
		env_name = "test/" + env_name;
		boost::filesystem::create_directories(boost::filesystem::path(env_name, boost::filesystem::native));
		::BerkelyDB::Environment env(env_name);
	}

	void BerkelyDB::createDatabaseInEnvironment()
	{
		std::string env_name(createTempName());
		env_name = "test/" + env_name;
		boost::filesystem::create_directories(boost::filesystem::path(env_name, boost::filesystem::native));
		::BerkelyDB::Environment env(env_name);
		::BerkelyDB::Database db(&env, createTempName());
	}
#endif

	void BerkelyDB::createDatabaseWithoutEnvironment()
	{
		std::string temp_name("test/");
		temp_name += createTempName();
		CPPUNIT_ASSERT_THROW(::BerkelyDB::Database db(0, temp_name), std::invalid_argument);
	}
	
#ifdef HAVE_BOOST_FILESYSTEM
	void BerkelyDB::checkEmptyDatabaseIterators()
	{
		std::string temp_name("test/");
		temp_name += createTempName();
		boost::filesystem::create_directories(boost::filesystem::path(temp_name, boost::filesystem::native));
		::BerkelyDB::Environment env(temp_name);
		::BerkelyDB::Database db(&env, createTempName());
		CPPUNIT_ASSERT_MESSAGE("On a freshly created database, the cursors begin and end should be the same", db.begin() == db.end());
	}

	void BerkelyDB::checkEmptyDatabaseAccessors()
	{
		std::string temp_name("test/");
		temp_name += createTempName();
		boost::filesystem::create_directories(boost::filesystem::path(temp_name, boost::filesystem::native));
		::BerkelyDB::Environment env(temp_name);
		temp_name = createTempName();
		::BerkelyDB::Database db(&env, temp_name);
		CPPUNIT_ASSERT_MESSAGE("On a freshly created database, the \"empty\" accessor should return true", db.empty());
		CPPUNIT_ASSERT_MESSAGE("On a freshly created database, the \"size\" accessor should return 0", db.size() == 0);
	}

	void BerkelyDB::createDatabaseAndReopen()
	{
		std::string temp_name("test/");
		temp_name += createTempName();
		boost::filesystem::create_directories(boost::filesystem::path(temp_name, boost::filesystem::native));
		::BerkelyDB::Environment env(temp_name);
		std::string db_name(createTempName());
		{
			::BerkelyDB::Database db(&env, db_name);
		}
		::BerkelyDB::Database db(&env, db_name);
	}

	void BerkelyDB::addValueToDatabase()
	{
		::BerkelyDB::KeyDataPair kdp("Key", "Data");
		::BerkelyDB::KeyDataPair kdp_empty("KeyEmpty", "");
		std::string temp_name("test/");
		temp_name += createTempName();
		boost::filesystem::create_directories(boost::filesystem::path(temp_name, boost::filesystem::native));
		::BerkelyDB::Environment env(temp_name);
		std::string db_name(createTempName());
		::BerkelyDB::Database db(&env, db_name);
		db.insert(kdp);
		db.insert(kdp_empty);
		CPPUNIT_ASSERT(db.size() == 2);
	}

	void BerkelyDB::addValueAndReadBackThroughCursor()
	{
		::BerkelyDB::KeyDataPair kdp("Key", "Data");
		std::string temp_name("test/");
		temp_name += createTempName();
		boost::filesystem::create_directories(boost::filesystem::path(temp_name, boost::filesystem::native));
		::BerkelyDB::Environment env(temp_name);
		std::string db_name(createTempName());
		::BerkelyDB::Database db(&env, db_name);
		db.insert(kdp);
		
		::BerkelyDB::Cursor beg(db.begin());
		::BerkelyDB::KeyDataPair kdp2(*beg);
		CPPUNIT_ASSERT(kdp == kdp2);
	}

	void BerkelyDB::addValueAndReadBackThroughCursor2()
	{
		/* Cursors should not invalidate for an insertion: if we take a 
		 * begin and an end cursor and insert something, begin should 
		 * point to the newly inserted entry without further modification */
		::BerkelyDB::KeyDataPair kdp2("Key2", "Data2");
		std::string temp_name("test/");
		temp_name += createTempName();
		boost::filesystem::create_directories(boost::filesystem::path(temp_name, boost::filesystem::native));
		::BerkelyDB::Environment env(temp_name);
		std::string db_name(createTempName());
		::BerkelyDB::Database db(&env, db_name);
		::BerkelyDB::Cursor beg(db.begin());
		db.insert(kdp2);

		::BerkelyDB::KeyDataPair kdp3(*beg);
		CPPUNIT_ASSERT(kdp2 == kdp3);
	}

	void BerkelyDB::addManyValuesAndReadBackThroughCursor()
	{
		/* Cursors should not invalidate for an insertion: if we take a 
		 * begin and an end cursor and insert something, begin should 
		 * point to the newly inserted entry without further modification */
		std::string temp_name("test/");
		temp_name += createTempName();
		boost::filesystem::create_directories(boost::filesystem::path(temp_name, boost::filesystem::native));
		::BerkelyDB::Environment env(temp_name);
		std::string db_name(createTempName());
		::BerkelyDB::Database db(&env, db_name);
		::BerkelyDB::Cursor beg(db.begin());

		{
			char * a("can break here to check memory usage before cache is filled");

			for (int index(0); index < 10000; ++index)
				db.insert(::BerkelyDB::KeyDataPair(index, 1));

			char * b("can break here to check memory usage once the cache is filled");

			for (int index(-1); index > -10000; --index)
				db.insert(::BerkelyDB::KeyDataPair(index, 1));

			char * c("can break here to check memory usage; should be the same as in b");

			{
				::BerkelyDB::Cursor current(db.begin());
				for (; current != db.end(); ++current)
				{
					::BerkelyDB::KeyDataPair key_data(*current);
				}
			}

			char * d("can break here to check memory usage; should be the same as in b");
		}
	}

	void BerkelyDB::writeThroughCursor()
	{
		::BerkelyDB::KeyDataPair kdp("Key", "Data");
		std::string temp_name("test/");
		temp_name += createTempName();
		boost::filesystem::create_directories(boost::filesystem::path(temp_name, boost::filesystem::native));
		::BerkelyDB::Environment env(temp_name);
		std::string db_name(createTempName());
		::BerkelyDB::Database db(&env, db_name);
		::BerkelyDB::Cursor beg(db.begin());
		*beg = kdp;
	}

	void BerkelyDB::writeThroughCursor2()
	{
		::BerkelyDB::KeyDataPair kdp("Key", "Data");
		std::string temp_name("test/");
		temp_name += createTempName();
		boost::filesystem::create_directories(boost::filesystem::path(temp_name, boost::filesystem::native));
		::BerkelyDB::Environment env(temp_name);
		std::string db_name(createTempName());
		::BerkelyDB::Database db(&env, db_name);
		::BerkelyDB::Cursor beg(db.begin());
		*beg = kdp;
		::BerkelyDB::Cursor beg2(db.begin());
		::BerkelyDB::KeyDataPair kdp2(*beg2);
		CPPUNIT_ASSERT(kdp == kdp2);
	}

	void BerkelyDB::writeThroughCursor3()
	{
		::BerkelyDB::KeyDataPair kdp1("Key", "Data1");
		::BerkelyDB::KeyDataPair kdp2("Key", "Data2");
		std::string temp_name("test/");
		temp_name += createTempName();
		boost::filesystem::create_directories(boost::filesystem::path(temp_name, boost::filesystem::native));
		::BerkelyDB::Environment env(temp_name);
		std::string db_name(createTempName());
		::BerkelyDB::Database db(&env, db_name);
		db.insert(kdp1);
		::BerkelyDB::Cursor beg(db.begin());
		*beg = kdp2;
		::BerkelyDB::Cursor beg2(db.begin());
		::BerkelyDB::KeyDataPair kdp3(*beg2);
		CPPUNIT_ASSERT(kdp2== kdp3);
	}

	void BerkelyDB::writeThroughCursor4()
	{
		::BerkelyDB::KeyDataPair kdp1("Key1", "Data1");
		::BerkelyDB::KeyDataPair kdp2("Key2", "Data2");
		std::string temp_name("test/");
		temp_name += createTempName();
		boost::filesystem::create_directories(boost::filesystem::path(temp_name, boost::filesystem::native));
		::BerkelyDB::Environment env(temp_name);
		std::string db_name(createTempName());
		::BerkelyDB::Database db(&env, db_name);
		db.insert(kdp1);
		::BerkelyDB::Cursor beg(db.begin());
		CPPUNIT_ASSERT_THROW(*beg = kdp2, ::BerkelyDB::Exceptions::CursorError);
	}

	void BerkelyDB::erase()
	{
		::BerkelyDB::KeyDataPair kdp1("Key1", "Data1");
		std::string temp_name("test/");
		temp_name += createTempName();
		boost::filesystem::create_directories(boost::filesystem::path(temp_name, boost::filesystem::native));
		::BerkelyDB::Environment env(temp_name);
		std::string db_name(createTempName());
		::BerkelyDB::Database db(&env, db_name);
		db.insert(kdp1);
		CPPUNIT_ASSERT(db.size() == 1);
		db.erase("Key1");
		CPPUNIT_ASSERT(db.empty());
		CPPUNIT_ASSERT(db.size() == 0);
	}

	void BerkelyDB::clear()
	{
		::BerkelyDB::KeyDataPair kdp1("Key1", "Data1");
		::BerkelyDB::KeyDataPair kdp2("Key2", "Data2");
		std::string temp_name("test/");
		temp_name += createTempName();
		boost::filesystem::create_directories(boost::filesystem::path(temp_name, boost::filesystem::native));
		::BerkelyDB::Environment env(temp_name);
		std::string db_name(createTempName());
		::BerkelyDB::Database db(&env, db_name);
		db.insert(kdp1);
		db.insert(kdp2);
		CPPUNIT_ASSERT(db.size() == 2);
		db.clear();
		CPPUNIT_ASSERT(db.empty());
		CPPUNIT_ASSERT(db.size() == 0);
	}

	void BerkelyDB::get()
	{
		std::string temp_name("test/");
		temp_name += createTempName();
		boost::filesystem::create_directories(boost::filesystem::path(temp_name, boost::filesystem::native));
		::BerkelyDB::Environment env(temp_name);
		std::string db_name(createTempName());
		::BerkelyDB::Database db(&env, db_name);

		::BerkelyDB::KeyDataPair kdp1("Key1", "Data1");
		::BerkelyDB::KeyDataPair kdp2("Key2", "Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2");

		db.insert(kdp1);
		db.insert(kdp2);
		CPPUNIT_ASSERT(db.size() == 2);

		::BerkelyDB::KeyDataPair result1("Key1", "");
		::BerkelyDB::KeyDataPair result2("Key2", "");
		::BerkelyDB::KeyDataPair result3("Key3", "");

		bool result_code1 = db.get(result1);
		CPPUNIT_ASSERT(result_code1 == true);
		CPPUNIT_ASSERT(result1 == ::BerkelyDB::KeyDataPair("Key1", "Data1"));

		bool result_code2 = db.get(result2);
		CPPUNIT_ASSERT(result_code2 == true);
		CPPUNIT_ASSERT(result2 == ::BerkelyDB::KeyDataPair("Key2", "Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2Data2"));

		bool result_code3 = db.get(result3);
		CPPUNIT_ASSERT(result_code3 == false);
		CPPUNIT_ASSERT(result3 == ::BerkelyDB::KeyDataPair("Key3", ""));
	}
#endif

	std::string BerkelyDB::createTempName()
	{
		/* Creating an environment shouldn't fail if the directory it is created in exists */
		// create a temporary directory from a subset of a randomly shuffled alphabet :)
		std::string alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
		static bool initialized__(false);
		if (!initialized__)
		{
			std::srand(static_cast< unsigned int >(std::time(0)));
			initialized__ = true;
		}
		else
		{ /* already initialized */ }
		std::random_shuffle(alphabet.begin(), alphabet.end());
		alphabet.resize(8);

		return alphabet;
	}
}

