Test_Depends_SRC :=				main.cpp \
						DirectedGraph.cpp 
						
SRC += $(patsubst %,tests/lib/Depends/%,$(Test_Depends_SRC))

Test_Depends_OBJ := $(patsubst %.cpp,tests/lib/Depends/%.lo,$(Test_Depends_SRC))

OBJ += $(Test_Depends_OBJ)

TEST_LDFLAGS=$(LDFLAGS) $(LIBS) -lcppunit
$(eval $(call LINK_BINARY_template,Test_Depends.bin,$(Test_Depends_OBJ),$(TEST_LDFLAGS)))
CHECK_DEPS += Test_Depends.bin 


