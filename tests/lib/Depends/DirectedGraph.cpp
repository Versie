#include "DirectedGraph.h"
#include <string>
#include <Depends/DirectedGraph.h>

CPPUNIT_TEST_SUITE_REGISTRATION( DirectedGraph );

void DirectedGraph::setUp()
{ /* no-op */ }

void DirectedGraph::tearDown()
{ /* no-op */ }

void DirectedGraph::tryDeclareGraph01()
{
	using Depends::DirectedGraph;

	DirectedGraph< std::string, void,  true,  true > graph01;
	DirectedGraph< std::string,  int,  true,  true > graph02;
	DirectedGraph< std::string, void, false,  true > graph03;
	DirectedGraph< std::string,  int, false,  true > graph04;
	DirectedGraph< std::string, void,  true, false > graph05;
	DirectedGraph< std::string,  int,  true, false > graph06;
	DirectedGraph< std::string, void, false, false > graph07;
	DirectedGraph< std::string,  int, false, false > graph08;
}

void DirectedGraph::example01()
{
	using Depends::DirectedGraph;

	DirectedGraph< std::string, int > graph;
	graph.insert(std::make_pair("first", "second"), 12);
	graph.insert(std::make_pair("first", "third"), 63);
	DirectedGraph< std::string, int >::Edges edges(graph.findEdges("first"));
	CPPUNIT_ASSERT(std::distance(edges.begin(), edges.end()) == 2);
	CPPUNIT_ASSERT(edges.begin()->source_ == (++(edges.begin()))->source_);
	CPPUNIT_ASSERT(edges.begin()->source_ == "first");
	edges = graph.findEdges("second");
	CPPUNIT_ASSERT(std::distance(edges.begin(), edges.end()) == 1);
	CPPUNIT_ASSERT(edges.begin()->target_ == "second");
	graph.insert(std::make_pair("third", "first"), 63);
}

void DirectedGraph::example02()
{
	using Depends::DirectedGraph;

	DirectedGraph< std::string, void, false, false > graph;
	graph.insert(std::make_pair("first", "second"));
	graph.insert(std::make_pair("first", "third"));
	DirectedGraph< std::string, void, false, false >::Edges edges(graph.findEdges("first"));
	CPPUNIT_ASSERT(std::distance(edges.begin(), edges.end()) == 2);
	CPPUNIT_ASSERT(edges.begin()->source_ == (++(edges.begin()))->source_);
	CPPUNIT_ASSERT(edges.begin()->source_ == "first");
	edges = graph.findEdges("second");
	CPPUNIT_ASSERT(std::distance(edges.begin(), edges.end()) == 1);
	CPPUNIT_ASSERT(edges.begin()->target_ == "second");
	graph.insert(std::make_pair("third", "first"));
}

void DirectedGraph::example03()
{
	using Depends::DirectedGraph;

	DirectedGraph< std::string, int, true > graph;
	graph.insert(std::make_pair("first", "second"), 12);
	graph.insert(std::make_pair("first", "third"), 63);
	DirectedGraph< std::string, int, true >::Edges edges(graph.findEdges("first"));
	CPPUNIT_ASSERT(std::distance(edges.begin(), edges.end()) == 2);
	CPPUNIT_ASSERT(edges.begin()->source_ == (++(edges.begin()))->source_);
	CPPUNIT_ASSERT(edges.begin()->source_ == "first");
	edges = graph.findEdges("second");
	CPPUNIT_ASSERT(std::distance(edges.begin(), edges.end()) == 1);
	CPPUNIT_ASSERT(edges.begin()->target_ == "second");
	CPPUNIT_ASSERT_THROW(graph.insert(std::make_pair("third", "first"), 63), Depends::Exceptions::CircularPath);
}

void DirectedGraph::example04()
{
	using Depends::DirectedGraph;

	DirectedGraph< std::string, void, true, false > graph;
	graph.insert(std::make_pair("first", "second"));
	graph.insert(std::make_pair("first", "third"));
	DirectedGraph< std::string, void, true, false >::Edges edges(graph.findEdges("first"));
	CPPUNIT_ASSERT(std::distance(edges.begin(), edges.end()) == 2);
	CPPUNIT_ASSERT(edges.begin()->source_ == (++(edges.begin()))->source_);
	CPPUNIT_ASSERT(edges.begin()->source_ == "first");
	edges = graph.findEdges("second");
	CPPUNIT_ASSERT(std::distance(edges.begin(), edges.end()) == 1);
	CPPUNIT_ASSERT(edges.begin()->target_ == "second");
	CPPUNIT_ASSERT_THROW(graph.insert(std::make_pair("third", "first")), Depends::Exceptions::CircularPath);
}
