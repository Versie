#ifndef _tests_depends_directedgraph_h
#define _tests_depends_directedgraph_h

#include <cppunit/extensions/HelperMacros.h>

class DirectedGraph : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE( DirectedGraph );
	CPPUNIT_TEST(tryDeclareGraph01);
	CPPUNIT_TEST(example01);
	CPPUNIT_TEST(example02);
	CPPUNIT_TEST(example03);
	CPPUNIT_TEST(example04);
	CPPUNIT_TEST_SUITE_END();

public:
	virtual void setUp();
	virtual void tearDown();

protected:
	void tryDeclareGraph01();
	void example01();
	void example02();
	void example03();
	void example04();
private:
};

#endif
