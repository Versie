libBerkelyDB.la : $(BerkelyDB_OBJ)
	$(LIBTOOL) --mode=link $(CXX) -version-info 0:0:0 -rpath $(prefix)/lib $(LDFLAGS) -o $@ $^ -ldb_cxx

libVersie.la : $(Versie_OBJ) libBerkelyDB.la
	$(LIBTOOL) --mode=link $(CXX) -version-info 0:0:0 -rpath $(prefix)/lib $(LDFLAGS) -o $@ $^ 

versie.bin : $(versie_OBJ) libVersie.la
	$(LIBTOOL) --mode=link $(CXX) -o $@ $(filter-out libVersie.la,$^) -lVersie 

Tests.bin : $(Tests_OBJ) libVersie.la
	$(LIBTOOL) --mode=link $(CXX) -o $@ $(filter-out libVersie.la,$^) -lVersie -lcppunit -lltdl $(LIBS)
