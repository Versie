#ifndef berkelydb_operation_h
#define berkelydb_operation_h

#include "Details/prologue.h"
#include <boost/scoped_ptr.hpp>
#include <string>
#include <map>

namespace BerkelyDB
{
	class Environment;
	class Transaction;

	/** Represent an atomic operation to be performed on the environment inside a database
	 * transaction. */
	class BERKELYDB_API Operation
	{
	public:
		Operation(Environment & env, bool no_wait = false);

		/** Perform the operation by creating a database transaction and calling performTransaction. If a deadlock
		 * occurs within the call to performTransaction and a BerkelyDB::Exceptions::Deadlock escapes, it will be
		 * caught and the operation will be attempted again by calling performTransaction once more, unless the 
		 * someone set the retry flag to false.
		 *
		 * Override this function if you wish to perform additional non-database steps before or after the 
		 * transaction. */
		virtual void perform();

		unsigned getRetryCount() const;
		bool getRetryFlag() const;
		void setRetryFlag(bool retry);

	protected:
		virtual ~Operation();

		/** Perform the actual operation inside a database transaction. This function 
		 * should not affect any state that won't be rollbacked when the database
		 * transaction aborts. For changing states outside of the database, override perform
		 * and do it before or after the transaction. */
		virtual void performTransaction(Transaction & transaction) = 0;

	private:
		Environment & env_;
		bool no_wait_;
		unsigned retry_count_;
		bool retry_;
	};
}

#endif