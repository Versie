#ifndef _berkelydb_cursor_h
#define _berkelydb_cursor_h

#include "Details/prologue.h"

#include "Details/KeyDataPairProxy.h"
#include "Transaction.h"
#include <boost/shared_ptr.hpp>

namespace BerkelyDB
{
	class Database;
	class KeyDataPair;

	//! Wrap a cursor in much the same way as an iterator would work
	/** This class represents a cursor in the database. In many ways, a
	 * cursor is just like a Forward Iterator and I've tried to have it
	 * model one as closely as possible. However, in order to completely
	 * model a forward iterator we'd have to seriously slow down the 
	 * database access, which is therefore optional. For example: cursors
	 * are not comparable by default: only past-the-end cursors will 
	 * compare equal to eachoter but cursors referring anywhere else
	 * in the database will always compare unequal unless the database
	 * was opened with the follow_record_numbers flag. */
	class BERKELYDB_API Cursor
	{
	public :
		Cursor(const Cursor & cursor);
		~Cursor();

		Cursor & operator=(Cursor cursor) { return swap(cursor); }

		//! Compare this cursor to another one
		/** Unless the follow_record_numbers was set on the database,
		 * this operator will only return true if both this cursor 
		 * and the cursor we compare with are at the end of the database. */
		bool operator==(const Cursor & rhs) const;
		bool operator!=(const Cursor & rhs) const { return !operator==(rhs); }

		//! pre-increment
		Cursor & operator++();
		//! post-increment
		Cursor operator++(int);

		Details::KeyDataPairProxy operator*() const { return get(); }
		Details::KeyDataPairProxy operator->() const { return get(); }
		Details::KeyDataPairProxy get() const;

		Details::KeyDataPairProxy operator*() { return get(); }
		Details::KeyDataPairProxy operator->() { return get(); }
		Details::KeyDataPairProxy get();

		void write(const KeyDataPair & kdp);

		Cursor & swap(Cursor & cursor) throw();

	private :
		struct Data;
		enum Position { begin__, end__ };
		
		Cursor(Database * owner, Position pos, bool constant, Transaction & transaction);
		bool atEnd() const;
		void initAtBegin() const;
		
		Database * owner_;
		boost::shared_ptr< Data > data_;
		mutable bool at_begin_uninitialized_;
		mutable bool at_end_;
		bool constant_;
		Transaction * transaction_;

		friend class Database;
	};
}

#endif

