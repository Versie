#ifndef berkelydb_storage_h
#define berkelydb_storage_h

#include "Details/prologue.h"
#include <boost/scoped_ptr.hpp>
#include <string>
#include "Storage-Cursor.h"
#include "Storage-Operator.h"

namespace BerkelyDB
{
	class Database;
	class Environment;

	template < typename KeyType, typename DataType >
	class Storage
	{
	public:
		typedef StorageCursor< KeyType, DataType > Cursor; // Defined in Storage-Cursor.h 
		typedef StorageConstOperator< KeyType, DataType > ConstOperator; // Defined in Storage-Operator.h
		typedef StorageOperator< KeyType, DataType > Operator; // Defined in Storage-Operator.h

		Storage(Environment & env, const std::string & name)
			: env_(env)
			, db_(new BerkelyDB::Database(&env, name + ".db",
				BerkelyDB::Database::create | // Create the database if it doesn't exist
				BerkelyDB::Database::auto_commit |
				BerkelyDB::Database::multithreaded |
				BerkelyDB::Database::disable_btree_reverse_split))
		{ /* no-op */ }

		~Storage()
		{ /* no-op */ }

		Environment & getEnvironment() const
		{
			return env_;
		}

	private:
		Environment & env_;
		boost::scoped_ptr< BerkelyDB::Database > db_;

		friend class StorageConstOperator< KeyType, DataType >;
		friend class StorageOperator< KeyType, DataType >;
		friend class StorageCursor< KeyType, DataType >;
	};
}

#endif