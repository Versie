#include "Operation.h"
#include "Environment.h"
#include "Storage.h"
#include "Transaction.h"
#include <BerkelyDB/Environment.h>
#include <BerkelyDB/Transaction.h>
#include <BerkelyDB/Exceptions/Deadlock.h>

namespace BerkelyDB
{
	Operation::Operation(Environment & env, bool no_wait)
		: env_(env),
		  no_wait_(no_wait),
		  retry_count_(0),
		  retry_(true)
	{ /* no-op */ }

	Operation::~Operation()
	{ /* no-op */ }

	void Operation::perform()
	{
		do
		{
			try
			{
				Transaction transaction(&env_, no_wait_);
				{
					performTransaction(transaction);
				}
				transaction.commit();
				retry_ = false;
			}
			catch (const BerkelyDB::Exceptions::Deadlock &)
			{
				++retry_count_;
			}
		}
		while (retry_);
	}

	unsigned Operation::getRetryCount() const
	{
		return retry_count_;
	}

	bool Operation::getRetryFlag() const
	{
		return retry_;
	}

	void Operation::setRetryFlag(bool retry)
	{
		retry_ = retry;
	}
}
