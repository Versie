#include "Cursor.h"
#include <cassert>
#ifdef HAVE_DB_CXX_H
#include <db_cxx.h>
#else
#ifdef HAVE_DB_DB_CXX_H
#include <db/db_cxx.h>
#else
#ifdef HAVE_DB4_DB_CXX_H
#include <db4/db_cxx.h>
#else
#error You need to define either HAVE_DB_CXX_H or HAVE_DB_DB_CXX_H or HAVE_DB4_DB_CXX_H
#endif
#endif
#endif
#include <vector>
#include "Database.h"
#include "Environment.h"
#include "KeyDataPair.h"
#include "Exceptions/CursorError.h"
#include "Exceptions/Deadlock.h"
#include "Details/KeyDataPairProxy.h"

namespace BerkelyDB
{
#include "Cursor/Data.ipp"
#include "Environment/Data.ipp"
#include "Database/Data.ipp"
#include "Transaction/Data.ipp"

	Cursor::Cursor(const Cursor & cursor)
	{
		owner_ = cursor.owner_;
		data_ = cursor.data_;
		at_begin_uninitialized_ = cursor.at_begin_uninitialized_;
		at_end_ = cursor.at_end_;
		constant_ = cursor.constant_;
		transaction_ = cursor.transaction_;
	}

	Cursor::~Cursor()
	{
		assert(owner_);
	}

	bool Cursor::operator==(const Cursor & rhs) const
	try
	{
		if (at_begin_uninitialized_)
		{
			initAtBegin();
			at_begin_uninitialized_ = false;
		}
		else
		{ /* already initialized */ }
		bool at_end(atEnd());
		bool rhs_at_end(rhs.atEnd());
		if (at_end&& !rhs_at_end)
			return false;
		else if (!at_end && rhs_at_end)
			return false;
		else if (at_end && rhs_at_end)
			return true;
		else
		{ /* we'll have to check where the cursors point */ }
	
		assert(owner_);
		if (!owner_->follow_record_numbers)
			return false;
		else
		{ /* no-op */ }

		assert(!at_end);
		assert(!rhs_at_end);
		assert(data_);
		assert(data_->cursor_);

		unsigned long lhs_id(0);
		Dbt lhs_key;
		Dbt lhs_data(&lhs_id, sizeof(unsigned long));
		unsigned long rhs_id(0);
		Dbt rhs_key;
		Dbt rhs_data(&rhs_id, sizeof(unsigned long));
		if (data_->cursor_->get(&lhs_key, &lhs_data, DB_CURRENT | DB_GET_RECNO) != 0)
			throw Exceptions::CursorError(Exceptions::CursorError::dereference_failed);
		else
		{ /* all is well */ }
		if (rhs.data_->cursor_->get(&rhs_key, &rhs_data, DB_CURRENT | DB_GET_RECNO) != 0)
			throw Exceptions::CursorError(Exceptions::CursorError::dereference_failed);
		else
		{ /* all is well */ }	
		return lhs_id == rhs_id;
	}
	catch (const DbDeadlockException &)
	{
		throw Exceptions::Deadlock();
	}

	Cursor & Cursor::operator++()
	try
	{
		if (at_begin_uninitialized_)
		{
			initAtBegin();
			at_begin_uninitialized_ = false;
		}
		else
		{ /* already initialized */ }
		if (!atEnd())
		{
			Dbt key;
			Dbt data;
			int rv (data_->cursor_->get(&key, &data, DB_NEXT));
			if (rv != 0 && rv != DB_NOTFOUND)
				throw Exceptions::CursorError(Exceptions::CursorError::increment_failed);
			else if (rv == DB_NOTFOUND)
				at_end_ = true;
			else
			{ /* incremented - all is well */ }
		}
		else
			throw Exceptions::CursorError(Exceptions::CursorError::increment_failed);

		return *this;
	}
	catch (const DbDeadlockException &)
	{
		throw Exceptions::Deadlock();
	}

	Cursor Cursor::operator++(int)
	{
		Cursor old(*this);
		++(*this);
		return old;
	}

	Details::KeyDataPairProxy Cursor::get() const
	try
	{
		if (at_begin_uninitialized_)
		{
			initAtBegin();
			at_begin_uninitialized_ = false;
		}
		else
		{ /* already initialized */ }
		if (atEnd())
			throw Exceptions::CursorError(Exceptions::CursorError::dereference_failed);
		else
		{ /* not at the end - all is well */ }
		Dbt key;
		Dbt data;

		data_->cursor_->get(&key, &data, DB_CURRENT);
		std::vector< unsigned char > key_buffer(
				static_cast<unsigned char *>(key.get_data()), static_cast<unsigned char *>(key.get_data()) + key.get_size());
		std::vector< unsigned char > data_buffer(
				static_cast<unsigned char *>(data.get_data()), static_cast<unsigned char *>(data.get_data()) + data.get_size());
		
		std::auto_ptr< KeyDataPair > kdp(new KeyDataPair(key_buffer.begin(), key_buffer.end(), data_buffer.begin(), data_buffer.end()));
		return Details::KeyDataPairProxy(*this, kdp);
	}
	catch (const DbDeadlockException &)
	{
		throw Exceptions::Deadlock();
	}
	
	Details::KeyDataPairProxy Cursor::get()
	try
	{
		if (at_begin_uninitialized_)
		{
			initAtBegin();
			at_begin_uninitialized_ = false;
		}
		else
		{ /* already initialized */ }
		if (atEnd())
		{
			/* at the end. Only a back-inserting cursor can be created in that case */
			return Details::KeyDataPairProxy(*this);
		}
		else
		{ /* not at the end - all is well */ }
		Dbt key;
		Dbt data;

		data_->cursor_->get(&key, &data, DB_CURRENT);
		std::vector< unsigned char > key_buffer(
			static_cast<unsigned char *>(key.get_data()), static_cast<unsigned char *>(key.get_data()) + key.get_size());
		std::vector< unsigned char > data_buffer(
			static_cast<unsigned char *>(data.get_data()), static_cast<unsigned char *>(data.get_data()) + data.get_size());

		std::auto_ptr< KeyDataPair > kdp(new KeyDataPair(key_buffer.begin(), key_buffer.end(), data_buffer.begin(), data_buffer.end()));
		return Details::KeyDataPairProxy(*this, kdp);
	}
	catch (const DbDeadlockException &)
	{
		throw Exceptions::Deadlock();
	}

	void Cursor::write(const KeyDataPair & kdp)
	{
		if (atEnd())
			owner_->insert(kdp);
		else
		{	// write through the cursor
			std::vector< char > key_data(kdp.getKeySize());
			kdp.readKey(key_data.begin());
			std::vector< char > data_data(kdp.getDataSize());
			kdp.readData(data_data.begin());

			/* Dbc::oyt ignores the key value. I find this rather misleading 
			 * and will check that they key value is the same as the current 
			 * one in stead, and throw if it isn't. */
			KeyDataPair current_kdp(get());
			std::vector< char > current_key_data(current_kdp.getKeySize());
			current_kdp.readKey(current_key_data.begin());
			if (key_data != current_key_data)
				throw Exceptions::CursorError(Exceptions::CursorError::write_failed);
			else
			{ /* all is well */ }

			Dbt key(&key_data[0], key_data.size());
			Dbt data(&data_data[0], data_data.size());
			if (data_->cursor_->put(&key, &data, DB_CURRENT) != 0)
				throw Exceptions::CursorError(Exceptions::CursorError::write_failed);
			else
			{ /* all is well */ }
		}
	}
	
	Cursor & Cursor::swap(Cursor & cursor) throw()
	{
		owner_ = cursor.owner_;
		data_ = cursor.data_;
		at_begin_uninitialized_ = cursor.at_begin_uninitialized_;
		at_end_ = cursor.at_end_;
		constant_ = cursor.constant_;
		transaction_ = cursor.transaction_;
		return *this;
	}
						   
	Cursor::Cursor(Database * owner, Position pos, bool constant, Transaction & transaction)
		: owner_(owner),
		  data_(new Data),
		  at_begin_uninitialized_(pos == begin__),
		  at_end_(pos == end__),
		  constant_(constant),
		  transaction_(&transaction)
	{
		assert(owner);
	}

	bool Cursor::atEnd() const
	try
	{
		if (!at_end_)
		{
			Dbt key;
			Dbt data;
			if (data_->cursor_->get(&key, &data, DB_CURRENT) == DB_NOTFOUND &&	// current is no longer value and
			    data_->cursor_->get(&key, &data, DB_NEXT) == DB_NOTFOUND)		// next doesn't exist
				at_end_ = true;
			else
			{ /* not at the end yet */ }
		}
		else
		{ /* nothing to do here */ }
		
		return at_end_;
	}
	catch (const DbDeadlockException &)
	{
		throw Exceptions::Deadlock();
	}

	void Cursor::initAtBegin() const
	try
	{
		owner_->data_->db_.cursor(transaction_->data_->txn_, &(data_->cursor_), constant_ ? 0 : DB_WRITECURSOR);
		Dbt key;
		Dbt data;
		if (data_->cursor_->get(&key, &data, DB_FIRST) == DB_NOTFOUND)
			at_end_ = true;
		else
		{ /* not at the end */ }
	}
	catch (const DbDeadlockException &)
	{
		throw Exceptions::Deadlock();
	}
}


