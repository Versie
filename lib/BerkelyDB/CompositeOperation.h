#ifndef berkelydb_compositeoperation_h
#define berkelydb_compositeoperation_h

#include "Details/prologue.h"
#include "Operation.h"
#include <boost/ptr_container/ptr_list.hpp>
#include "Details/Component.h"

namespace BerkelyDB
{
	class Environment;
	class Storage;

	/** Atomic database operation built from multiple components executed in sequence. */
	class BERKELYDB_API CompositeOperation : public Operation
	{
	public:
		typedef Details::Components Components_;
		typedef Details::SequenceState< Components_ > SequenceState;
		typedef Details::Component Component; // backward compatibility with previous versions

		CompositeOperation(Environment & env, bool no_wait = false);
		~CompositeOperation();

		void add(std::auto_ptr< Component > component);
		void clear();
		bool empty() const;
		size_t size() const;
		
		/** Call perform on the first component, which will, after recursively calling perform
		 * on each element, call performTransaction. If no component has been added, does nothing. */
		void perform();

	protected:
		/** Call performTransaction on each component. Normally, performTransaction is called by
		 * the last component's perform function. */
		void performTransaction(Transaction & transaction);

	private:
		Components_ components_;

		friend struct Details::SequenceState< Components_ >;
	};
}

#endif