#include "CompositeOperation.h"

namespace BerkelyDB
{
	void CompositeOperation::Component::perform(SequenceState & sequence_state)
	{
		if (sequence_state.current_itr_ != sequence_state.end_itr_)
			(++sequence_state.current_itr_)->perform(sequence_state);
		else
			sequence_state.op_.Operation::perform();
	}

	CompositeOperation::CompositeOperation(Environment & env, bool no_wait)
		: Operation(env, no_wait)
	{ /* no-op */ }

	CompositeOperation::~CompositeOperation()
	{ /* no-op */ }

	void CompositeOperation::add(std::auto_ptr< Component > component)
	{
		components_.push_back(component.release());
	}

	void CompositeOperation::clear()
	{
		components_.clear();
	}

	bool CompositeOperation::empty() const
	{
		return components_.empty();
	}

	size_t CompositeOperation::size() const
	{
		return components_.size();
	}

	void CompositeOperation::perform()
	{
		if (!components_.empty())
		{
			SequenceState sequence_state(*this);
			sequence_state.current_itr_->perform(sequence_state);
		}
		else
			Operation::perform();
	}

	void CompositeOperation::performTransaction(Transaction & transaction)
	{
		for (Components_::iterator curr(components_.begin()); curr != components_.end(); ++curr)
			curr->performTransaction(transaction);
	}
}
