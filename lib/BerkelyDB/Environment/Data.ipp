struct Environment::Data
{
	Data()
		: env_(0)
	{
		env_.set_alloc(std::malloc, std::realloc, std::free);
	}

	DbEnv env_;
};
