struct Transaction::Data
{
	Data(Environment * environment, unsigned flags)
		: txn_(0)
	{
		unsigned db_flags = 0;
		if (flags & no_wait)
		{
			db_flags |= DB_TXN_NOWAIT;
		}
		else
		{ /* transaction will wait */ }

		environment->data_->env_.txn_begin(0, &txn_, db_flags);
	}

	// Empty transaction, used for the Transaction::no_transaction__ constant.
	Data()
		: txn_(0)
	{ /* no-op */ }
	
	DbTxn * txn_;
};
