#ifndef berkelydb_queuestorage_cursor_h
#define berkelydb_queuestorage_cursor_h

#include "Details/prologue.h"
#include "QueueStorage.h"
#include "Storage-Cursor.h"

namespace BerkelyDB
{
	// QueueStorage implemented on top of the Storage class, but this is inefficient
	// and should be changed to a non-tree BerkelyDB container.
	template < typename DataType >
	class QueueStorageCursor : private Storage::Cursor< std::string, DataType >
	{
	public:
		QueueStorageCursor(Transaction & transaction, const Storage< std::string, DataType > & storage)
			: StorageCursor< std::string, DataType >(transaction, storage)
		{ /* no-op */ }

		QueueStorageCursor(const ConstOperator & storage_operator)
			: StorageCursor< std::string, DataType >(storage_operator)
		{ /* no-op */ }

		QueueStorageCursor(const Operator & storage_operator)
			: StorageCursor< std::string, DataType >(storage_operator)
		{ /* no-op */ }

		using StorageCursor::value;
		using StorageCursor::atEnd;
		using StorageCursor::advance;
	};
}
#endif