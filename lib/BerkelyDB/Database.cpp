#include "Database.h"
#ifdef HAVE_DB_CXX_H
#include <db_cxx.h>
#else
#ifdef HAVE_DB_DB_CXX_H
#include <db/db_cxx.h>
#else
#ifdef HAVE_DB4_DB_CXX_H
#include <db4/db_cxx.h>
#else
#error You need to define either HAVE_DB_CXX_H or HAVE_DB_DB_CXX_H or HAVE_DB4_DB_CXX_H
#endif
#endif
#endif
#include <algorithm>
#include <cerrno>
#include <vector>
#include <cstdlib>
#include "Environment.h"
#include "KeyDataPair.h"
#include "Details/KeyDataPairProxy.h"
#include "Exceptions/DuplicateKey.h"
#include "Exceptions/NotFound.h"
#include "Exceptions/Deadlock.h"
#include <iostream>

namespace BerkelyDB
{
#include "Environment/Data.ipp"
#include "Database/Data.ipp"
#include "Transaction/Data.ipp"
		
	struct Database::DatabaseAdditionGuard
	{
		DatabaseAdditionGuard(Environment * environment, Database * database)
			: environment_(environment)
			, database_(database)
			, dismissed_(false)
		{ /* no-op */ }

		~DatabaseAdditionGuard()
		{
			if (!dismissed_)
				environment_->removeDatabase(database_);
			else
			{ /* dismissed */ }
		}

		void dismiss()
		{
			dismissed_ = true;
		}

		Environment * environment_;
		Database * database_;
		bool dismissed_;
	};

	Database::Database(Environment * environment, const std::string & filename, int mode/* = create*/, size_t page_size/* = 0 */)
		: data_(new Data(environment)),
		  filename_(filename),
		  environment_(environment),
		  follow_record_numbers_(false),
		  mode_(mode)
	{
		if (!environment)
			throw std::invalid_argument("We need an environment because that is the only way we have to tell the database implementation how to allocate memory!");
		else
		{ /* pre-condition met */ }

		environment->addDatabase(this);
		DatabaseAdditionGuard guard(environment, this);

		u_int32_t flags(0);
		if (mode & create)
			flags |= DB_CREATE;
		else
		{ /* fail if it doesn't exist */ }
		if (mode & read_only)
			flags |= DB_RDONLY;
		else
		{ /* we can write... */ }
		if (mode & follow_record_numbers)
		{
			flags |= DB_RECNUM;		// need this for Cursor::operator==
			follow_record_numbers_ = true;
		}
		else
		{ /* don't fully support Cursor::operator== */ }
		if (mode & auto_commit)
		{
			if (!environment || !(environment->getMode() & Environment::transactions))
				throw std::invalid_argument("Environment not transactional");
			else
			{ /* all is well */ }
			flags |= DB_AUTO_COMMIT; // using an autocommit transaction for opening the database
		}
		else
		{ /* don't auto-commit */ }
		if (mode & multithreaded)
		{
			flags |= DB_THREAD;
		}
		else
		{ /* not multithreaded */ }

		try
		{
			if (page_size != 0)
				data_->db_.set_pagesize(page_size);
			else
			{ /* use default page size */ }

			if (mode & disable_btree_reverse_split)
				data_->db_.set_flags(DB_REVSPLITOFF);
			else
			{ /* btree reverse splits enabled */ }

			data_->db_.open(
				NULL,			// no transaction pointer
				filename.c_str(),	// database filename
				NULL,			// no optional logical database name
				DB_BTREE,		// using a BTREE for access
				flags,			// flags to open
				0			// use default file modes
			);
		}
		catch (const DbException & e)
		{
			throw std::runtime_error(e.what());
		}

		guard.dismiss();
	}
			
	Database::~Database()
	{
		close();
	}

	Cursor Database::begin(Transaction & transaction)
	{
		Cursor retval(this, Cursor::begin__, (environment_ ? ((environment_->getMode() & Environment::transactions) ? true : false) : false), transaction);
		return retval;
	}

	Cursor Database::end(Transaction & transaction)
	{
		Cursor retval(this, Cursor::end__, (environment_ ? ((environment_->getMode() & Environment::transactions) ? true : false) : false), transaction);
		return retval;
	}
	
	bool Database::get(KeyDataPair & kdp, Transaction & transaction)
	try
	{
		assert(data_);

		unsigned key_size(kdp.getKeySize());

		std::vector< char > key_data(key_size);
		kdp.readKey(key_data.begin());

		Dbt key(&key_data[0], key_data.size());
		Dbt data;
		data.set_flags(DB_DBT_MALLOC);

		int rv(data_->db_.get(transaction.data_->txn_, &key, &data, DB_RMW));
		if (rv == 0)
		{
			const char * begin(static_cast< const char * >(data.get_data()));
			const char * end(begin + data.get_size());
			kdp.writeData(begin, end);
			free(data.get_data());
			data.set_data(0);
			return true;
		}
		else switch (rv)
		{
		case DB_NOTFOUND:
			return false;
		case DB_LOCK_DEADLOCK:
			throw Exceptions::Deadlock();
		case DB_BUFFER_SMALL:
			/* the requested item could not be returned due to undersized buffer */
		case DB_REP_HANDLE_DEAD:
			/* The database handle has been invalidated because a replication election unrolled a committed transaction. */
		case DB_REP_LOCKOUT:
			/* The operation was blocked by client/master synchronization. */
		case DB_SECONDARY_BAD:
			/* A secondary index references a nonexistent primary key. */
		case EINVAL:
			/* If a record number of 0 was specified; the DB_THREAD flag was specified to the Db::open method and none of the DB_DBT_MALLOC, DB_DBT_REALLOC or DB_DBT_USERMEM flags were set in the Dbt; the Db::pget method was called with a Db handle that does not refer to a secondary index; or if an invalid flag value or parameter was specified. */
		default:
			throw std::bad_alloc(); // TODO
		}
	}
	catch (const DbDeadlockException &)
	{
		throw Exceptions::Deadlock();
	}

	bool Database::empty(Transaction & transaction) const
	try
	{
		/* The implementation of this function may be a bit weird, but it's 
		 * what the BerkelyDB documentation wants us to do:
		 * int DB->stat(DB *db, DB_TXN *txnid, void *sp, u_int32_t flags);
		 * The DB->stat method creates a statistical structure and copies a 
		 * pointer to it into user-specified memory locations. Specifically,
		 * if sp is non-NULL, a pointer to the statistics for the database 
		 * are copied into the memory location to which it refers.
		 *
		 * Statistical structures are stored in allocated memory. If 
		 * application-specific allocation routines have been declared 
		 * (see DB_ENV->set_alloc for more information) [this is the case
		 * in this library, which is why databases must be allocated with 
		 * an environemtn], they are used to allocate the memory; (...).
		 * The caller is responsible for deallocating the memory. To deallocate 
		 * the memory, free the memory reference; references inside the 
		 * returned memory need not be individually freed.
		 *
		 * Hence, we pass a pointer to this function to a pointer of type
		 * DB_BTREE_STAT*. The library will fill in the pointer, and we will
		 * free the associated memory with std::free. */
		assert(data_);
		DB_BTREE_STAT * stats(0);
		int rv(data_->db_.stat(transaction.data_->txn_, &stats, 0));
		// as of here, the code will not throw
		bool retval(!rv && stats->bt_nkeys == 0 && stats->bt_ndata == 0);
		std::free(stats);
		return retval;
	}
	catch (const DbDeadlockException &)
	{
		throw Exceptions::Deadlock();
	}

	size_t Database::size(Transaction & transaction) const
	try
	{
		/* The implementation of this function may be a bit weird, but it's 
		 * what the BerkelyDB documentation wants us to do:
		 * int DB->stat(DB *db, DB_TXN *txnid, void *sp, u_int32_t flags);
		 * The DB->stat method creates a statistical structure and copies a 
		 * pointer to it into user-specified memory locations. Specifically,
		 * if sp is non-NULL, a pointer to the statistics for the database 
		 * are copied into the memory location to which it refers.
		 *
		 * Statistical structures are stored in allocated memory. If 
		 * application-specific allocation routines have been declared 
		 * (see DB_ENV->set_alloc for more information) [this is the case
		 * in this library, which is why databases must be allocated with 
		 * an environemtn], they are used to allocate the memory; (...).
		 * The caller is responsible for deallocating the memory. To deallocate 
		 * the memory, free the memory reference; references inside the 
		 * returned memory need not be individually freed.
		 *
		 * Hence, we pass a pointer to this function to a pointer of type
		 * DB_BTREE_STAT*. The library will fill in the pointer, and we will
		 * free the associated memory with std::free. */
		assert(data_);
		DB_BTREE_STAT * stats(0);
		int rv(data_->db_.stat(transaction.data_->txn_, &stats, 0));
		// as of here, the code will not throw
		size_t retval(stats->bt_ndata);
		std::free(stats);
		return retval;
	}
	catch (const DbDeadlockException &)
	{
		throw Exceptions::Deadlock();
	}

	void Database::insert(const KeyDataPair & kdp, bool overwrite, Transaction & transaction)
	try
	{
		unsigned key_size(kdp.getKeySize());
		if (key_size == 0)
			throw std::runtime_error("Cannot insert: empty key");
		else
		{ /* key not empty */ }

		std::vector< char > key_data(key_size);
		kdp.readKey(key_data.begin());
		std::vector< char > data_data(kdp.getDataSize());
		kdp.readData(data_data.begin());
		
		Dbt key(&key_data[0], key_data.size());
		Dbt data(data_data.empty() ? 0 : &data_data[0], data_data.size());

		int rv(data_->db_.put(transaction.data_->txn_, &key, &data, overwrite ? 0 : DB_NOOVERWRITE));
		switch (rv)
		{
		case EACCES :
			// An attempt was made to modify a read-only database.
			/* TODO */
		case DB_REP_HANDLE_DEAD :
			// The database handle has been invalidated because a
			// replication election unrolled a committed transaction.
			/* TODO */
#ifdef DB_REP_LOCKOUT
		case DB_REP_LOCKOUT :
			// The operation was blocked by client/master synchronization.
			/* TODO */
#endif
		case EINVAL :
			// If a record number of 0 was specified; an attempt was made
			// to add a record to a fixed-length database that was too
			// large to fit; an attempt was made to do a partial put;
			// an attempt was made to add a record to a secondary index;
			// or if an invalid flag value or parameter was specified.
			/* TODO */
		case ENOSPC :
			// A btree exceeded the maximum btree depth (255).
			throw std::bad_alloc();
		case DB_KEYEXIST :
			throw Exceptions::DuplicateKey();
		case DB_LOCK_DEADLOCK :
			throw Exceptions::Deadlock();
		default :
			if (rv != 0)
				throw std::bad_alloc(); // TODO
			else
			{ /* all is well */ }
		}
	}
	catch (const DbDeadlockException &)
	{
		throw Exceptions::Deadlock();
	}

	void Database::erase(const char * key, Transaction & transaction)
	{
		erase(KeyDataPair(key, ""), transaction);
	}

	void Database::erase(const KeyDataPair & kdp, Transaction & transaction/* = Transaction::no_transaction__*/)
	try
	{
		unsigned key_size(kdp.getKeySize());
		if (key_size == 0)
			throw std::runtime_error("Cannot erase: empty key");
		else
		{ /* key not empty */ }

		std::vector< char > key_data(key_size);
		kdp.readKey(key_data.begin());
		
		Dbt key(&key_data[0], key_data.size());

		int rv(data_->db_.del(transaction.data_->txn_, &key, 0));
		switch (rv)
		{
		case EACCES :
			// An attempt was made to modify a read-only database.
			/* TODO */
		case DB_REP_HANDLE_DEAD :
			// The database handle has been invalidated because a
			// replication election unrolled a committed transaction.
			/* TODO */
#ifdef DB_REP_LOCKOUT
		case DB_REP_LOCKOUT :
			// The operation was blocked by client/master synchronization.
			/* TODO */
#endif
		case EINVAL :
			// If a record number of 0 was specified; an attempt was made
			// to add a record to a fixed-length database that was too
			// large to fit; an attempt was made to do a partial put;
			// an attempt was made to add a record to a secondary index;
			// or if an invalid flag value or parameter was specified.
			/* TODO */
		case ENOSPC :
			// A btree exceeded the maximum btree depth (255).
			throw std::bad_alloc();
		case DB_NOTFOUND :
			throw Exceptions::NotFound();
		case DB_LOCK_DEADLOCK :
			throw Exceptions::Deadlock();
		default :
			if (rv != 0)
				throw std::bad_alloc(); // TODO
			else
			{ /* all is well */ }
		}
	}
	catch (const DbDeadlockException &)
	{
		throw Exceptions::Deadlock();
	}
	
	void Database::erase(const Details::KeyDataPairProxy & kdp, Transaction & transaction/* = Transaction::no_transaction__*/)
	{
		erase(KeyDataPair(kdp), transaction);
	}

	void Database::clear(Transaction & transaction)
	try
	{
		u_int32_t count;
		int rv(data_->db_.truncate(transaction.data_->txn_, &count, 0));
		switch (rv)
		{
		case EACCES :
			// An attempt was made to modify a read-only database.
			/* TODO */
		case DB_REP_HANDLE_DEAD :
			// The database handle has been invalidated because a
			// replication election unrolled a committed transaction.
			/* TODO */
#ifdef DB_REP_LOCKOUT
		case DB_REP_LOCKOUT :
			// The operation was blocked by client/master synchronization.
			/* TODO */
#endif
		case EINVAL :
			// There are open cursors in the database, or an invalid flag or 
			// parameter was specified.
			/* TODO */
		case DB_LOCK_DEADLOCK :
			throw Exceptions::Deadlock();
		case DB_LOCK_NOTGRANTED :
			/* TODO */
		default :
			if (rv != 0)
				throw std::bad_alloc(); // TODO
			else
			{ /* all is well */ }
		}
	}
	catch (const DbDeadlockException &)
	{
		throw Exceptions::Deadlock();
	}
	
	void Database::close()
	{
		if (data_)
			data_->db_.close(0);
		else
		{ /* no data_ - already closed */ }
		delete data_;
		data_ = 0;
		if (environment_)
			environment_->removeDatabase(this);
		else
		{ /* not registered in an environment */ }
	}
}


