# the library
BerkelyDB_SRC = Database.cpp \
		Environment.cpp \
		Cursor.cpp \
		KeyDataPair.cpp \
		Details/KeyDataPairProxy.cpp

BerkelyDB_INSTALL_HEADERS += $(patsubst %.cpp,%.h,$(BerkelyDB_SRC))
SRC += $(patsubst %,lib/BerkelyDB/%,$(BerkelyDB_SRC))

INSTALL_HEADERS += $(patsubst %,lib/BerkelyDB/%,$(BerkelyDB_INSTALL_HEADERS))
BerkelyDB_OBJ := $(patsubst %.cpp,lib/BerkelyDB/%.lo,$(BerkelyDB_SRC))
OBJ += $(BerkelyDB_OBJ)

$(eval $(call LINK_LIBRARY_template,libBerkelyDB.la,$(BerkelyDB_OBJ)))
