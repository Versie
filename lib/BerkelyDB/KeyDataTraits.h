#ifndef _berkelydb_keydatatraits_h
#define _berkelydb_keydatatraits_h

#include <memory>
#include "KeyDataPair.h"

namespace
{
	struct Empty { Empty() {} };
}
namespace BerkelyDB
{
	template < typename Serializable >
	struct KeyDataTraits
	{
		typedef std::auto_ptr< Serializable > AutoPointer;
		typedef void * Pointer;
		typedef const void * ConstPointer;

		static unsigned long getSize(const Serializable * serializable)
		{
			return sizeof(Serializable);
		}

		static Pointer getPointer(Serializable * serializable)
		{
			return serializable;
		}

		static ConstPointer getPointer(const Serializable * serializable)
		{
			return serializable;
		}
		
		static Serializable & assign(Serializable & lhs, const Serializable rhs)
		{
			return lhs = rhs;
		}

		static Serializable * create()
		{
			return new Serializable;
		}

		static void destroy(Serializable * serializable)
		{
			delete serializable;
		}

		static Serializable construct(size_t size)
		{
			return Serializable();
		}
	};

	template < typename Serializable >
	struct KeyTraits : public KeyDataTraits< Serializable >
	{
		typedef typename KeyDataTraits< Serializable >::AutoPointer	AutoPointer;
		typedef typename KeyDataTraits< Serializable >::Pointer		Pointer;
		typedef typename KeyDataTraits< Serializable >::ConstPointer	ConstPointer;

		using KeyDataTraits< Serializable >::getSize;
		using KeyDataTraits< Serializable >::getPointer;
		using KeyDataTraits< Serializable >::assign;
		using KeyDataTraits< Serializable >::create;
		using KeyDataTraits< Serializable >::destroy;
	};

	template < typename Serializable >
	struct DataTraits : public KeyDataTraits< Serializable >
	{
		typedef typename KeyDataTraits< Serializable >::AutoPointer	AutoPointer;
		typedef typename KeyDataTraits< Serializable >::Pointer		Pointer;
		typedef typename KeyDataTraits< Serializable >::ConstPointer	ConstPointer;

		using KeyDataTraits< Serializable >::getSize;
		using KeyDataTraits< Serializable >::getPointer;
		using KeyDataTraits< Serializable >::assign;
		using KeyDataTraits< Serializable >::create;
		using KeyDataTraits< Serializable >::destroy;
	};

	template <>
	struct KeyDataTraits< std::string >
	{
		typedef std::auto_ptr< std::string > AutoPointer;
		typedef void * Pointer;
		typedef const void * ConstPointer;

		static unsigned long getSize(const std::string * str)
		{
			return static_cast< unsigned long >(str->size());
		}

		static Pointer getPointer(std::string * str)
		{
			return &(*str)[0];
		}

		static ConstPointer getPointer(const std::string * str)
		{
			return &(*str)[0];
		}
		static std::string & assign(std::string & lhs, const std::string rhs)
		{
			return lhs = rhs;
		}

		static std::string * create()
		{
			return new std::string;
		}

		static void destroy(std::string * str)
		{
			delete str;
		}

		static std::string construct(size_t size)
		{
			return std::string(size, 0);
		}
	};

	template <>
	struct KeyDataTraits< std::vector<char> >
	{
		typedef std::auto_ptr< std::vector<char> > AutoPointer;
		typedef void * Pointer;
		typedef const void * ConstPointer;

		static unsigned long getSize(const std::vector<char> * bytes)
		{
			return static_cast< unsigned long >(bytes->size());
		}

		static Pointer getPointer(std::vector<char> * bytes)
		{
			return &(*bytes)[0];
		}

		static ConstPointer getPointer(const std::vector<char> * bytes)
		{
			return &(*bytes)[0];
		}
		static std::vector<char> & assign(std::vector<char> & lhs, const std::vector<char> rhs)
		{
			return lhs = rhs;
		}

		static std::vector<char> * create()
		{
			return new std::vector<char>;
		}

		static void destroy(std::vector<char> * bytes)
		{
			delete bytes;
		}

		static std::vector<char> construct(size_t size)
		{
			return std::vector<char>(size, 0);
		}
	};

	template <>
	struct KeyDataTraits< Empty >
	{
		typedef std::auto_ptr< std::string > AutoPointer;
		typedef void * Pointer;
		typedef const void * ConstPointer;

		static unsigned long getSize(const Empty * empty) { return 0; }
		static Pointer getPointer(Empty * empty) { return empty; }
		static ConstPointer getPointer(const Empty * empty) { return empty; }
		static Empty & assign(Empty & lhs, const Empty rhs) { }
		static Empty * create() { return new Empty(); }
		static void destroy(Empty * empty) { delete empty; }
		static Empty construct(size_t size) { return Empty(); }
	};
} 

#endif

