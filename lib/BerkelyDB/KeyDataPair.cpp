#include "KeyDataPair.h"
#include <algorithm>
#include <string>

namespace BerkelyDB
{
	namespace
	{
		template < typename Container >
		unsigned int readFromContainer(Container & container, unsigned long & read_offset, void * buffer, unsigned long buffer_size)
		{
			if (read_offset >= container.size())
			{
				read_offset = 0;
				return 0;
			}
			else
			{
				typename Container::const_iterator start(container.begin());
				typename Container::const_iterator end(container.end());
				std::advance(start, read_offset);
				
				assert(std::distance(start, end) > 0);
				if (static_cast< unsigned long >(std::distance(start, end)) > buffer_size)
				{
					end = start;
					std::advance(end, buffer_size);
				}
				else
				{ /* we can write everything into the buffer as is */ }
				const std::size_t to_read(std::distance(start, end));
				assert(to_read <= buffer_size);
	
				std::copy(start, end, static_cast<unsigned char *>(buffer));
				read_offset += to_read;
				return buffer_size;
			}
		}
	}
	
	struct KeyDataPair::Data
	{
		typedef std::vector< unsigned char > Vector;
	
		Data()
			: key_read_offset_(0),
			  key_write_offset_(0),
			  data_read_offset_(0),
			  data_write_offset_(0)
		{ /* no-op */ }

		Vector key_;
		mutable unsigned long key_read_offset_;	// how many bytes have already been consumed of the current key
		unsigned long key_write_offset_;
		Vector data_;
		mutable unsigned long data_read_offset_;	// how many bytes have already been consumed of the current entry
		unsigned long data_write_offset_;
	};

	KeyDataPair::KeyDataPair()
		: data_(makeData())
	{ /* no-op */ }

	KeyDataPair::KeyDataPair(const KeyDataPair & kdp)
		: data_(new Data(*kdp.data_))
	{ /* no-op */ }

	KeyDataPair::KeyDataPair(const char * key, const char * data)
		: data_(makeData())
	{
		std::string _key(key);
		std::string _data(data);
		writeKey(_key.c_str(), _key.size());
		writeData(_data.c_str(), _data.size());
	}

	KeyDataPair::~KeyDataPair()
	{
		delete data_;
	}

	unsigned long KeyDataPair::getKeySize() const
	{
		return static_cast< unsigned long >(data_->key_.size());
	}
	
	unsigned long KeyDataPair::getDataSize() const
	{
		return static_cast< unsigned long >(data_->data_.size());
	}
	
	bool KeyDataPair::operator==(const KeyDataPair & rhs) const
	{
		// to be equal, the two KeyDataPairs must have both equal keys and datas in them (or be the same instance)
		if (this == &rhs)
			return true;
		else
		{ /* not the same instance, carry on */ }
		return data_->key_ == rhs.data_->key_ &&
		       data_->data_ == rhs.data_->data_;
	}

	KeyDataPair::Data * KeyDataPair::makeData()
	{
		return new Data;
	}

	unsigned long KeyDataPair::getCurrentKeySize() const
	{
		return static_cast< unsigned long >(data_->key_.size());
	}
	
	unsigned long KeyDataPair::readKey(void * buffer, unsigned long buffer_size) const
	{
		return readFromContainer(data_->key_, data_->key_read_offset_, buffer, buffer_size);
	}
	
	unsigned long KeyDataPair::writeKey(const void * buffer, unsigned long buffer_size)
	{
		const unsigned char * whence(static_cast<const unsigned char *>(buffer));
		std::copy(whence, whence + buffer_size, std::back_inserter(data_->key_));

		return buffer_size;
	}

	void * KeyDataPair::getRawData() const
	{
		return static_cast< void* >(&data_->data_[0]);
	}

	unsigned long KeyDataPair::getCurrentDataSize() const
	{
		return static_cast< unsigned long >(data_->data_.size());
	}
	
	unsigned long KeyDataPair::readData(void * buffer, unsigned long buffer_size) const
	{
		return readFromContainer(data_->data_, data_->data_read_offset_, buffer, buffer_size);
	}
	
	unsigned long KeyDataPair::writeData(const void * buffer, unsigned long buffer_size)
	{
		const unsigned char * whence(static_cast<const unsigned char *>(buffer));
		std::copy(whence, whence + buffer_size, std::back_inserter(data_->data_));

		return buffer_size;
	}

	
	bool operator!=(const KeyDataPair & lhs, const KeyDataPair & rhs)
	{
		return (!(lhs == rhs));
	}
}

