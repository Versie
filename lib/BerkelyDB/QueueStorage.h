#ifndef berkelydb_queuestorage_h
#define berkelydb_queuestorage_h

#include "Details/prologue.h"
#include "Storage.h"

namespace BerkelyDB
{
	class Database;
	class Environment;
	template < typename DataType >
	class QueueStorageOperator;
	template < typename DataType >
	class QueueStorageConstOperator;
	template < typename DataType >
	class QueueStorageCursor;

	// QueueStorage implemented on top of the Storage class, but this is inefficient
	// and should be changed to a non-tree BerkelyDB container.
	template < typename DataType >
	class QueueStorage : private Storage< std::string, DataType >
	{
	public:
		typedef QueueStorageCursor< DataType > Cursor; // Defined in QueueStorage-Cursor.h 
		typedef QueueStorageConstOperator< DataType > ConstOperator; // Defined in QueueStorage-Operator.h
		typedef QueueStorageOperator< DataType > Operator; // Defined in QueueStorage-Operator.h

		QueueStorage(Environment & env, const std::string & name)
			: Storage< DataType, DataType >(env, name + ".queue")
		{ /* no-op */ }

		~QueueStorage()
		{ /* no-op */ }

		using Storage::getEnvironment;
	};
}

#endif