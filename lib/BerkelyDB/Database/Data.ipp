struct Database::Data
{
	Data(Environment * environment)
		: db_(environment ? &(environment->data_->env_) : 0, 0)
	{
		if (!environment) db_.set_alloc(std::malloc, std::realloc, std::free);
	}
	
	Db db_;
};
