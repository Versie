#include "Transaction.h"
#ifdef HAVE_DB_CXX_H
#include <db_cxx.h>
#else
#ifdef HAVE_DB_DB_CXX_H
#include <db/db_cxx.h>
#else
#ifdef HAVE_DB4_DB_CXX_H
#include <db4/db_cxx.h>
#else
#error You need to define either HAVE_DB_CXX_H or HAVE_DB_DB_CXX_H or HAVE_DB4_DB_CXX_H
#endif
#endif
#endif
#include <algorithm>
#include <cerrno>
#include <vector>
#include <cstdlib>
#include "Environment.h"
#include "KeyDataPair.h"
#include "Exceptions/DuplicateKey.h"

namespace BerkelyDB
{
#include "Environment/Data.ipp"
#include "Transaction/Data.ipp"
	
	Transaction Transaction::no_transaction__;
		
	Transaction::Transaction(Environment * environment, unsigned flags)
		: flags_(flags),
		  data_(new Data(environment, flags)),
		  done_(false),
		  environment_(environment)
	{ /* no-op */ }
			
	Transaction::~Transaction()
	{
		if (!done_ && !(flags_ & no_auto_abort))
			data_->txn_->abort();
		else
		{ /* nothing more to do */ }

		delete data_;
	}

	void Transaction::commit()
	{
		if (done_)
			throw std::logic_error("Transaction already commited or aborted, cannot commit.");
		else
		{ /* can commit */ }

		done_ = true;
		data_->txn_->commit(0);
	}

	void Transaction::abort()
	{
		if (done_)
			throw std::logic_error("Transaction already commited or aborted, cannot abort.");
		else
		{ /* can abort */ }

		done_ = true;
		data_->txn_->abort();
	}
		
	Transaction::Transaction()
		: flags_(0),
		  data_(new Data()),
		  done_(true),
		  environment_(0)
	{ /* no-op */ }
}


