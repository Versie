#ifndef _berkelydb_database_h
#define _berkelydb_database_h

#include "Details/prologue.h"

#include <string>
#include "Cursor.h"

namespace BerkelyDB
{
	class Environment;
	class Cursor;
	class KeyDataPair;
	class Transaction;

	//! Class to wrap a handle to a database file
	//! \todo we don't provide get_open_flags, remove or rename, as Berkely DB does
	class BERKELYDB_API Database
	{
	public :
		//! Flags that can be used to open a database
		enum Flags {
			//! Create the database if it doesn't exist yet
			create		= 1,
			//! Open the database in read-only mode
			read_only	= 2,
			//! Follow record numbers
			/** \warning Doing this may, in some cases, seriously slow down
			 *           the database but is necessary if you want the
			 *           Cursor's operator== to work on anything but an
			 *           iterator pointing to the end. Most of the time,
			 *           you shouldn't need that, so don't use this option.
			 *           
			 * If you create the database with this flag, you must use the
			 * flag every time you open it. If you don't create the database
			 * with this flag, you cannot use it on the database at a later 
			 * time. */
			follow_record_numbers = 4,
			/** Automatically commit any changes.
			 * Only possible if the environment is transactional. */
			auto_commit = 8,
			/** Make the database usable by multiple concurent threads */
			multithreaded = 0x10,
			/** Prevent a btree database from being reduced to a minimum of pages.
			 * Useful to decrease deadlock occurences in a database constantly growing 
			 * and shrinking. */
			disable_btree_reverse_split,
		};
				
		//! Construct a handle from the name of the file
		/** As Berkely databases reside in files, this
		 * class opens the file with the Berkely back-end
		 * and will close it when destroyed. This enables
		 * you to use a Berkely database with RAII */
		Database(Environment * environment, const std::string & filename, int mode = create, size_t page_size = 0);
		~Database();

		//! Get a cursor to the start of a sequence containing everything in the database
		Cursor begin(Transaction & transaction = Transaction::no_transaction__);
		Cursor end(Transaction & transaction = Transaction::no_transaction__);

		//! Get data for the key in the given key-data pair, and fill the key-data pair with this data.
		//! If the key is not present in the database, return false.
		bool get(KeyDataPair & kdp, Transaction & transaction = Transaction::no_transaction__);

		bool empty(Transaction & transaction = Transaction::no_transaction__) const;

		size_t size(Transaction & transaction = Transaction::no_transaction__) const;

		void insert(const KeyDataPair & kdp, bool overwrite = false, Transaction & transaction = Transaction::no_transaction__);

		template < typename KeyType >
		void erase(const KeyType & key, Transaction & transaction = Transaction::no_transaction__)
		{
			struct Empty { Empty() {} };
			erase(KeyDataPair(key, Empty()), transaction);
		}

		void erase(const char * key, Transaction & transaction = Transaction::no_transaction__);
		void erase(const KeyDataPair & kdp, Transaction & transaction = Transaction::no_transaction__);
		void erase(const Details::KeyDataPairProxy & kdp, Transaction & transaction = Transaction::no_transaction__);

		void clear(Transaction & transaction = Transaction::no_transaction__);

		int getMode() const { return mode_; }

	private :
		struct Data;
		struct DatabaseAdditionGuard;

		// Neither CopyConstructible nor Assignable
		Database(const Database &);
		Database & operator=(const Database &);
		
		void init();
		void close();

		Data * data_;
		std::string filename_;
		Environment * environment_;
		bool follow_record_numbers_;
		int mode_;

		friend class Environment;
		friend class Cursor;
	};
}

#endif

