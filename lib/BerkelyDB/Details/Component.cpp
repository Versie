#include "Component.h"

namespace BerkelyDB
{
	namespace Details
	{
		Component * new_clone(const Component & r)
		{
			return r.clone();
		}

		void delete_clone( const Component * r )
		{
			delete r;
		}
	}
}

