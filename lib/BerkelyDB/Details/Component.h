#ifndef versie_berkelydb_details_component_h
#define versie_berkelydb_details_component_h

#include "Components.h"
#include "SequenceState.h"

namespace BerkelyDB
{
	class Transaction;
	namespace Details
	{
		class Component
		{
		public:
			/** Override this function if you need to perform non-database steps before or after the transaction.
			* If you override, don't forget to call Component::perform from inside your function, which is 
			* responsible for calling perform on the next component and performTransaction for each component. */
			virtual void perform(SequenceState< Components > & sequence_state);

			/** Perform the actual operation inside a database transaction. This function 
			* should not affect any state that won't be rollbacked when the database
			* transaction aborts. For changing states outside of the database, override perform
			* and do it before or after the transaction. */
			virtual void performTransaction(Transaction & transaction) = 0;

			virtual Component * clone() const;
		};

		Component * new_clone(const Component & r);
		void delete_clone( const Component * r );

	}
}

#endif
