#ifndef _berkelydb_details_keydatapairproxy_h
#define _berkelydb_details_keydatapairproxy_h

#include "prologue.h"

#include <memory>

namespace BerkelyDB
{
	class Cursor;
	class KeyDataPair;
	
	namespace Details
	{
		class BERKELYDB_API KeyDataPairProxy
		{
		public :
			KeyDataPairProxy(const Cursor & owner, std::auto_ptr< KeyDataPair > kdp);
			KeyDataPairProxy(Cursor & owner, std::auto_ptr< KeyDataPair > kdp);
			KeyDataPairProxy(Cursor & owner);	// for back insertion
			KeyDataPairProxy(const KeyDataPairProxy & kdpp);	// move constructur
			~KeyDataPairProxy();

			operator const KeyDataPair & () const;
			const KeyDataPair * operator->() const;

			KeyDataPairProxy & operator=(const KeyDataPair & rhs);

		private :
			// not Assignable
			KeyDataPairProxy & operator=(const KeyDataPairProxy & kdpp);
			
			const Cursor * const_cursor_;
			Cursor * mutable_cursor_;

			mutable KeyDataPair * kdp_;
		};
	}
}

#endif

