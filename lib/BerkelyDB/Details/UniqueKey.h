#ifndef berkelydb_details_uniquekey_h
#define berkelydb_details_uniquekey_h

namespace BerkelyDB
{
	namespace Details
	{
#pragma pack(push)
#pragma pack(1)
		struct UniqueKey
		{
		public:
			UniqueKey();
			UniqueKey(const UniqueKey & other);
			UniqueKey & operator=(const UniqueKey & other);

			double getTime() const;
			unsigned getSerialNumber() const;

		private:
			double time_;
			unsigned serial_number_;

			static unsigned current_serial_number__;
		};
#pragma pack(pop)
	}
}

#endif