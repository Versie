#include "UniqueKey.h"
#include <ctime>
#include <boost/detail/endian.hpp>
#include <boost/static_assert.hpp>

namespace BerkelyDB
{
	namespace Details
	{
		namespace
		{
			template < typename DataType >
			DataType swapEndianness(DataType value)
			{
				union
				{
					DataType d;
					unsigned char b[sizeof(DataType)];
				}
				input, output;
				
				input.d = value;
				for (unsigned index(0); index < sizeof(DataType); ++index)
					output.b[index] = input.b[sizeof(DataType)-index-1];

				return output.d;
			}

			template < typename DataType >
			inline DataType systemToBigEndian(DataType little_endian_value)
			{
#ifdef BOOST_LITTLE_ENDIAN
				return swapEndianness(little_endian_value);
#endif
#ifdef BOOST_BIG_ENDIAN
				return value;
#endif
			}

			template < typename DataType >
			inline DataType bigToSystemEndian(DataType little_endian_value)
			{
#ifdef BOOST_LITTLE_ENDIAN
				return swapEndianness(little_endian_value);
#endif
#ifdef BOOST_BIG_ENDIAN
				return value;
#endif
			}
		}

		UniqueKey::UniqueKey()
			: time_(systemToBigEndian(time(0))), 
			  serial_number_(systemToBigEndian(++current_serial_number__))
		{
			// no-op, but we need to check that at compile time and it's difficult to place it elsewhere.
			// We're checking that the alignment pragmas are working properly and that there is no unused
			// space in our struct between variables which could stay uninitialized.
			BOOST_STATIC_ASSERT(sizeof(UniqueKey) == sizeof(time_) + sizeof(serial_number_));
		}

		UniqueKey::UniqueKey(const UniqueKey & other) 
			: time_(other.time_), 
			  serial_number_(other.serial_number_)
		{ /* no-op */ }

		UniqueKey & UniqueKey::operator=(const UniqueKey & other)
		{
			time_ = other.time_;
			serial_number_ = other.serial_number_;
			return *this;
		}

		double UniqueKey::getTime() const
		{
			return bigToSystemEndian(time_);
		}

		unsigned UniqueKey::getSerialNumber() const
		{
			return bigToSystemEndian(serial_number_);
		}

		unsigned UniqueKey::current_serial_number__(0);
	}
}