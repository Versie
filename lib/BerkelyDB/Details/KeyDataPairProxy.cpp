#include "KeyDataPairProxy.h"
#include "../KeyDataPair.h"
#include "../Cursor.h"
#include "../Exceptions/CursorError.h"
#include "../Exceptions/KeyDataPairError.h"

namespace BerkelyDB
{
	namespace Details
	{
		KeyDataPairProxy::KeyDataPairProxy(const Cursor & owner, std::auto_ptr< KeyDataPair > kdp)
			: const_cursor_(&owner),
			  mutable_cursor_(0),
			  kdp_(kdp.release())
		{ /* no-op */ }
		
		KeyDataPairProxy::KeyDataPairProxy(Cursor & owner, std::auto_ptr< KeyDataPair > kdp)
			: const_cursor_(0),
			  mutable_cursor_(&owner),
			  kdp_(kdp.release())
		{ /* no-op */ }

		KeyDataPairProxy::KeyDataPairProxy(Cursor & owner)
			: const_cursor_(0),
			  mutable_cursor_(&owner),
			  kdp_(0)
		{ /* no-op */ }

		KeyDataPairProxy::KeyDataPairProxy(const KeyDataPairProxy & kdpp)
			: const_cursor_(kdpp.const_cursor_),
			  mutable_cursor_(kdpp.mutable_cursor_),
			  kdp_(kdpp.kdp_)
		{
			kdpp.kdp_ = 0;
		}

		KeyDataPairProxy::~KeyDataPairProxy()
		{
			delete kdp_;
		}

		KeyDataPairProxy::operator const KeyDataPair & () const
		{
			if (!kdp_)
				throw Exceptions::CursorError(Exceptions::CursorError::dereference_failed);
			else
				return *kdp_;
		}
		
		const KeyDataPair * KeyDataPairProxy::operator->() const
		{
			if (!kdp_)
				throw Exceptions::CursorError(Exceptions::CursorError::dereference_failed);
			else
				return kdp_;
		}

		KeyDataPairProxy & KeyDataPairProxy::operator=(const KeyDataPair & rhs)
		{
			if (mutable_cursor_)
			{
				if (kdp_)
					*kdp_ = rhs;
				else
					kdp_ = new KeyDataPair(rhs);
				mutable_cursor_->write(*kdp_);
			}
			else
				throw Exceptions::KeyDataPairError(Exceptions::KeyDataPairError::assign_const);
			return *this;
		}
	}
}


