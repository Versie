#ifndef versie_berkelydb_details_components_h
#define versie_berkelydb_details_components_h

#include <boost/ptr_container/ptr_list.hpp>

namespace BerkelyDB
{
	namespace Details
	{
		class Component;
		typedef boost::ptr_list< Component > Components;
	}
}

#endif
