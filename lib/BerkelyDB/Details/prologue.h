#ifndef _berkelydb_details_prologue_h
#define _berkelydb_details_prologue_h

#ifndef BERKELYDB_API
#	ifdef _MSC_VER
#		ifndef BERKELYDB_EXPORTS
#			define BERKELYDB_API __declspec(dllimport)
#			ifndef VERSIE_SLN
#				define AUTOLINK_LIB_NAME BerkelyDB
#				define AUTOLINK_LIB_VERSION "2"
#				include <Utilities/Details/autolink.h>
#				undef AUTOLINK_LIB_VERSION
#				undef AUTOLINK_LIB_NAME
#			endif
#		else
#			define BERKELYDB_API __declspec(dllexport)
#		endif
#	else
#		define BERKELYDB_API
#	endif
#endif

#endif
