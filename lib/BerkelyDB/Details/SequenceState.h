#ifndef versie_berkelydb_details_sequencestate_h
#define versie_berkelydb_details_sequencestate_h

namespace BerkelyDB
{
	class CompositeOperation;

	namespace Details
	{
		template < typename Components_ >
		struct SequenceState
		{
			SequenceState(CompositeOperation & op)
				: op_(op)
				, current_itr_(op.components_.begin())
				, end_itr_(op.components_.begin())
			{ /* no-op */ }

			CompositeOperation & op_;
			typename Components_::iterator current_itr_;
			typename Components_::iterator end_itr_;
		};
	}
}

#endif
