#ifndef _berkelydb_exceptions_deadlock_h
#define _berkelydb_exceptions_deadlock_h

#include "../Details/prologue.h"
#include <stdexcept>

namespace BerkelyDB
{
	namespace Exceptions
	{
		class BERKELYDB_API Deadlock : public std::exception
		{
		public:
			Deadlock()
				: std::exception("Deadlock occured during a database operation.")
			{ /* no-op */ }
		};
	}
}

#endif

