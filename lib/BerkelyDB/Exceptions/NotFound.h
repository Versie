#ifndef _berkelydb_exceptions_notfound_h
#define _berkelydb_exceptions_notfound_h

#include "../Details/prologue.h"
#include <stdexcept>

namespace BerkelyDB
{
	namespace Exceptions
	{
		class BERKELYDB_API NotFound : public std::exception
		{
		public:
			NotFound()
				: std::exception("The specified key could not be found.")
			{ /* no-op */ }
		};
	}
}

#endif

