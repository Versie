#ifndef _berkelydb_exceptions_duplicatekey_h
#define _berkelydb_exceptions_duplicatekey_h

#include "../Details/prologue.h"
#include <stdexcept>

namespace BerkelyDB
{
	namespace Exceptions
	{
		struct BERKELYDB_API DuplicateKey : public std::runtime_error
		{
			DuplicateKey() : std::runtime_error("Duplicate key") { /* no-op */ }
		};
	}
}

#endif

