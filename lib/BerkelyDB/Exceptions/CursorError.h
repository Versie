#ifndef _berkelydb_exceptions_cursorerror_h
#define _berkelydb_exceptions_cursorerror_h

#include "../Details/prologue.h"
#include <stdexcept>

namespace BerkelyDB
{
	namespace Exceptions
	{
		class BERKELYDB_API CursorError : public std::runtime_error
		{
		public :
			enum What { duplication_failed, dereference_failed, increment_failed, write_failed };

			CursorError(What code)
				: std::runtime_error(getErrorMessage(code))
			{ /* no-op */ }

		private :
			static const char * getErrorMessage(What what)
			{
				switch (what)
				{
				case duplication_failed :
					return "Cursor duplication failed";
				case dereference_failed :
					return "Failed to dereference cursor";
				default :
					return "Unknown error";
				}
			}
		};
	}
}

#endif

