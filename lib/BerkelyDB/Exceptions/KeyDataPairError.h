#ifndef _berkelydb_exceptions_keydatapairerror_h
#define _berkelydb_exceptions_keydatapairerror_h

#include "../Details/prologue.h"
#include <stdexcept>

namespace BerkelyDB
{
	namespace Exceptions
	{
		struct BERKELYDB_API KeyDataPairError : public std::runtime_error
		{
			enum Why { size_mismatch, assign_const };

			KeyDataPairError(Why why)
				: std::runtime_error(getErrorMessage(why)), why_(why)
			{ /* no-op */ }

			Why why_;

		private:
			static const char * getErrorMessage(Why why)
			{
				switch (why)
				{
				case size_mismatch:
					return "Size mismatch";
				case assign_const:
					return "Assign const";
				default:
					return "Unknown error";
				}
			}
		};
	}
}

#endif

