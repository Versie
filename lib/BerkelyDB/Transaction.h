#ifndef _berkelydb_transaction_h
#define _berkelydb_transaction_h

#include "Details/prologue.h"

#include <string>

namespace Tests { class Transactions; }
namespace BerkelyDB
{
	class Environment;

	//! Class to wrap a transaction.
	class BERKELYDB_API Transaction
	{
	public :
		enum Flags
		{
			//! Prevent the uncommitted transaction from aborting when the destructor is called
			no_auto_abort = 1,
			//! Prevent transaction from waiting for a lock; if a lock is not available, throw 
			//! immediately
			no_wait = 2,
		};

		//! Create a new transaction inside an environment.
		Transaction(Environment * environment, unsigned flags = 0);
		~Transaction();

		void commit();
		void abort();

		static Transaction no_transaction__;

	protected :
		//! Constructor for no_transaction__; protected so that subclasses
		//! can create their own no_transaction__ constant.
		Transaction();

	private :
		//! Neither copy-constructible or assignable.
		Transaction(const Transaction &);
		Transaction & operator=(const Transaction &);

	private :
		struct Data;

		unsigned flags_;
		Data * data_;
		bool done_;
		Environment * environment_;

		friend class Environment;
		friend class Database;
		friend class Cursor;
		friend class Operation;
		friend class Tests::Transactions;
	};
}

#endif

