#ifndef _berkelydb_keydatapair_h
#define _berkelydb_keydatapair_h

#include "Details/prologue.h"

#include <cassert>
#include <iterator>
#include <vector>
#include <boost/mpl/if.hpp>
#include <boost/type_traits/is_same.hpp>
#include "KeyDataTraits.h"
#include "Exceptions/KeyDataPairError.h"

namespace BerkelyDB
{
	//! Storage unit
	/** Data, in the database, is stored as pairs of a key and some data.
	 * To allow as much flexability as possible, we store a wee bit of 
	 * metadata with the actual key/data data in the database in order
	 * to know how to re-construct the real types and allow for as much
	 * type-safety as possible. The meta-data we store consists of:
	 * \li the size of the stored data
	 *
	 * Together with the actual data, this allows you to store more than
	 * one chunk of data, of more than one type, in a single field in the
	 * database. */
	class BERKELYDB_API KeyDataPair
	{
	public :
		//! Construct an empty key-data pair
		KeyDataPair();
		//! Copy constructor
		KeyDataPair(const KeyDataPair & kdp);
		//! Construct a key-data pair with a given key and a given (chunk of) data
		/** Note that, although this class does allow you to store
		 * more than one key with more than one chunk of data in a
		 * single field of the database, we consider the entire
		 * key-set an data-set as a single key and a single data-unit
		 * when it comes to storing them in the database. 
		 * \param key the key to store
		 * \param data the data to store */
		template < typename KeyType, typename DataType >
		KeyDataPair(const KeyType & key, const DataType & data)
			: data_(makeData())
		{
			setKey(key);
			setData(data);
		}

		template < typename InputIterator1, typename InputIterator2 >
		KeyDataPair(InputIterator1 key_begin, InputIterator1 key_end, InputIterator2 data_begin, InputIterator2 data_end)
			: data_(makeData())
		{
			writeKey(key_begin, key_end);
			writeData(data_begin, data_end);
		}

		KeyDataPair(const char * key, const char * data);

		~KeyDataPair();

		//! Get the total size of the key stored
		/** As compound keys are considered as a single key, if you've created a
		 * compound key this will return the complete size of the key
		 * \returns the total size of the (compound) key stored in this object */
		unsigned long getKeySize() const;
		template < typename OutputIterator >
		void readKey(OutputIterator where) const
		{
			typedef typename boost::mpl::if_< typename boost::is_same< typename std::iterator_traits< OutputIterator >::value_type, void >::type, char, typename std::iterator_traits< OutputIterator >::value_type >::type ValueType;

			const std::size_t value_type_size(sizeof( ValueType ));
			const unsigned long total_size(getKeySize());
			for (unsigned long read_size(0); read_size / value_type_size < total_size / value_type_size; read_size += value_type_size)
			{
				ValueType temp;
				readKey(&temp, sizeof(temp));
				*where++ = temp;
			}
		}
		template < typename InputIterator >
		void writeKey( InputIterator begin, InputIterator end )
		{
			const std::size_t value_type_size(sizeof( typename std::iterator_traits< InputIterator >::value_type ));
			for ( ; begin != end; ++begin )
				writeKey(&(*begin), value_type_size);
		}
		template < typename KeyType >
		KeyType getKey() const
		{
			KeyType key(KeyTraits< KeyType >::construct(getCurrentKeySize()));
			if (getCurrentKeySize() != KeyTraits< KeyType >::getSize(&key))
				throw Exceptions::KeyDataPairError(Exceptions::KeyDataPairError::size_mismatch);
			else
			{ /* sizes concur */ }
			unsigned long size(KeyTraits< KeyType >::getSize(&key));
			typename KeyTraits< KeyType >::Pointer pointer(KeyTraits< KeyType >::getPointer(&key));
			unsigned long read(readKey(pointer, size));
			assert(read == size);

			return key;
		}
		
		template < typename KeyType >
		void setKey(const KeyType & key)
		{
			unsigned long size(KeyTraits< KeyType >::getSize(&key));
			typename KeyTraits< KeyType >::ConstPointer pointer(KeyTraits< KeyType >::getPointer(&key));
			unsigned long written(writeKey(pointer, size));
			assert(written == size);
		}

		//! Get the total size of the data
		//! \returns the total size of the data stored in this object
		unsigned long getDataSize() const;
		template < typename OutputIterator >
		void readData(OutputIterator where) const
		{
			typedef typename boost::mpl::if_< typename boost::is_same< typename std::iterator_traits< OutputIterator >::value_type, void >::type, char, typename std::iterator_traits< OutputIterator >::value_type >::type ValueType;

			const std::size_t value_type_size(sizeof( ValueType ));
			const unsigned long total_size(getDataSize());
			for (unsigned long read_size(0); read_size / value_type_size < total_size / value_type_size; read_size += value_type_size)
			{
				ValueType temp;
				readData(&temp, sizeof(temp));
				*where++ = temp;
			}
		}
		template < typename InputIterator >
		void writeData( InputIterator begin, InputIterator end )
		{
			const std::size_t value_type_size(sizeof( typename std::iterator_traits< InputIterator >::value_type ));
			for ( ; begin != end; ++begin )
				writeData(&(*begin), value_type_size);
		}
		template < typename DataType >
		DataType getData() const
		{
			DataType dat(DataTraits< DataType >::construct(getCurrentDataSize()));
			if (getCurrentDataSize() != DataTraits< DataType >::getSize(&dat))
				throw Exceptions::KeyDataPairError(Exceptions::KeyDataPairError::size_mismatch);
			else
			{ /* sizes concur */ }
			unsigned long size(DataTraits< DataType >::getSize(&dat));
			typename DataTraits< DataType >::Pointer pointer(DataTraits< DataType >::getPointer(&dat));
			unsigned long read(readData(pointer, size));
			assert(read == size);

			return dat;
		}
		void * getRawData() const;

		template < typename DataType >
		void setData(const DataType & data)
		{
			unsigned long size(DataTraits< DataType >::getSize(&data));
			typename DataTraits< DataType >::ConstPointer pointer(DataTraits< DataType >::getPointer(&data));
			unsigned long written(writeData(pointer, size));
			assert(written == size);
		}

		bool operator==(const KeyDataPair & rhs) const;

		KeyDataPair & operator=(KeyDataPair rhs)
		{ return swap(rhs); }
		KeyDataPair & swap(KeyDataPair & kdp) throw()
		{
			Data * temp(data_);
			data_ = kdp.data_;
			kdp.data_ = temp;
			return *this;
		}

	private :
		struct Data;

		static Data * makeData();
		unsigned long getCurrentKeySize() const;
		unsigned long readKey(void * buffer, unsigned long buffer_size) const;
		unsigned long writeKey(const void * buffer, unsigned long buffer_size);
		unsigned long getCurrentDataSize() const;
		unsigned long readData(void * buffer, unsigned long buffer_size) const;
		unsigned long writeData(const void * buffer, unsigned long buffer_size);
		
		Data * data_;
	};

	bool operator!=(const KeyDataPair & lhs, const KeyDataPair & rhs);
}

#include "KeyDataPair.hpp"

#endif

