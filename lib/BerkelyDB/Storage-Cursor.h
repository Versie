#ifndef berkelydb_storage_cursor_h
#define berkelydb_storage_cursor_h

#include "Details/prologue.h"
#include "Storage.h"
#include "Transaction.h"
#include <BerkelyDB/Cursor.h>
#include <string>

namespace BerkelyDB
{
	template < typename KeyType, typename DataType >
	class Storage;
	template < typename KeyType, typename DataType >
	class StorageConstOperator;
	template < typename KeyType, typename DataType >
	class StorageOperator;

	template < typename KeyType, typename DataType >
	class StorageCursor
	{
	public:
		StorageCursor(Transaction & transaction, const Storage< KeyType, DataType > & storage)
			: cursor_(pool.db_->begin(transaction))
			, end_(pool.db_->end(transaction))
		{ /* no-op */ }

		StorageCursor(const StorageConstOperator< KeyType, DataType > & pool_operator)
			: cursor_(pool_operator.storage_.db_->begin(pool_operator.transaction_))
			, end_(pool_operator.storage_.db_->end(pool_operator.transaction_))
		{ /* no-op */ }

		StorageCursor(const StorageOperator< KeyType, DataType > & storage_operator)
			: cursor_(pool_operator.storage_.db_->begin(storage_operator.transaction_))
			, end_(pool_operator.storage_.db_->end(storage_operator.transaction_))
		{ /* no-op */ }

		KeyType key()
		{
			return cursor_->getKey< std::string >();
		}

		DataType value()
		{
			return cursor_->getData< std::string >();
		}

		bool atEnd()
		{
			return cursor_ == end_;
		}

		bool advance()
		{
			++cursor_;
			return atEnd();
		}

	private:
		BerkelyDB::Cursor cursor_;
		BerkelyDB::Cursor end_;
	};
}

#endif