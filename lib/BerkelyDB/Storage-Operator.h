#ifndef berkelydb_storage_operator_h
#define berkelydb_storage_operator_h

#include "Details/prologue.h"
#include "Storage.h"
#include "Transaction.h"
#include <string>

namespace BerkelyDB
{
	template < typename KeyType, typename DataType >
	class StorageConstOperator
	{
	public:
		StorageConstOperator(Transaction & transaction, const Storage< KeyType, DataType > & storage)
			: transaction_(transaction)
			, storage_(storage)
		{ /* no-op */ }

		DataType get(const KeyType & key, bool & found)
		{
			BerkelyDB::KeyDataPair kdp(key, Empty());
			found = storage_.db_->get(kdp, transaction_);
			if (found)
				return kdp.getData< DataType >();
			else
				return DataType();
		}

		DataType get(const KeyType & key)
		{
			bool found;
			return get(key, found);
		}

		bool empty() const
		{
			return storage_.db_->empty(transaction_);
		}

		size_t size() const
		{
			return storage_.db_->size(transaction_);
		}

	protected:
		Transaction & transaction_;
		const Storage< KeyType, DataType > & storage_;

		friend StorageCursor< KeyType, DataType >;
	};

	template < typename KeyType, typename DataType >
	class StorageOperator
	{
	public:
		StorageOperator(Transaction & transaction, Storage< KeyType, DataType > & storage)
			: transaction_(transaction)
			, storage_(storage)
		{ /* no-op */ }

		bool empty() const
		{
			return storage_.db_->empty(transaction_);
		}

		size_t size() const
		{
			return storage_.db_->size(transaction_);
		}

		DataType get(const KeyType & key, bool & found)
		{
			BerkelyDB::KeyDataPair kdp(key, Empty());
			found = storage_.db_->get(kdp, transaction_);
			if (found)
				return kdp.getData< DataType >();
			else
				return DataType();
		}

		DataType get(const KeyType & key)
		{
			bool found;
			return get(key, found);
		}

		void insert(const KeyType & key, const DataType & value, bool overwrite = false)
		{
			storage_.db_->insert(BerkelyDB::KeyDataPair(key, value), overwrite, transaction_);
		}

		void erase(const KeyType & key)
		{
			storage_.db_->erase(key, transaction_);
		}

		void clear()
		{
			return storage_.db_->clear(transaction_);
		}

		operator StorageConstOperator< KeyType, DataType >() const
		{
			return StorageConstOperator< std::string, std::string >(transaction_, storage_);
		}

	protected:
		Transaction & transaction_;
		Storage< KeyType, DataType > & storage_;

		friend StorageCursor< KeyType, DataType >;
	};
}

#endif