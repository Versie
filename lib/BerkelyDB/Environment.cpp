#include "Environment.h"
#ifdef HAVE_DB_CXX_H
#include <db_cxx.h>
#else
#ifdef HAVE_DB_DB_CXX_H
#include <db/db_cxx.h>
#else
#ifdef HAVE_DB4_DB_CXX_H
#include <db4/db_cxx.h>
#else
#error You need to define either HAVE_DB_CXX_H or HAVE_DB_DB_CXX_H or HAVE_DB4_DB_CXX_H
#endif
#endif
#endif
#include <algorithm>
#include <cassert>
#include <cstdlib>
#include "Database.h"

namespace BerkelyDB
{
#include "Environment/Data.ipp"

	Environment::Environment(const std::string & path, int mode/* = create */, DeadlockPolicy deadlock_policy/* = abort_default */)
		: data_(new Data),
		  mode_(mode)
	{
		assert(data_);
		if ((mode & transactions) && (mode & writable_cursors))
			throw std::invalid_argument("Writable cursors are not supported with transactions");
		else if ((mode & recovery) && !(mode & transactions))
			throw std::invalid_argument("Transactions are required for recovery.");
		else
		{ /* all is well */ }
		u_int32_t flags(0);
		if (mode & create)
			flags |= DB_CREATE;
		else
		{ /* fail if the path doesn't exist */ }
		flags |= DB_INIT_MPOOL;	// always needed
		if (mode & transactions)
		{
			flags |= DB_INIT_TXN;   // enable transactions
			flags |= DB_INIT_LOCK;  // locking subsystem (needed for transactions)
			flags |= DB_INIT_LOG;   // logging subsystem (needed for transactions)
			flags |= DB_INIT_MPOOL; // enable memory pool (needed for transactions)
		}
		else
		{ /* No need for transactions */ }
		if (mode & writable_cursors)
			flags |= DB_INIT_CDB;	// needed for writable cursors
		else
		{ /* no support for writable cursors */ }
		if (mode & recovery)
		{
			flags |= DB_RECOVER;
		}
		else
		{ /* no support for writable cursors */ }
		if (mode & multithreaded)
		{
			flags |= DB_THREAD;
		}
		else
		{ /* no support for multi-threaded calls */ }
		if (mode & single)
		{
			flags |= DB_PRIVATE;
		}
		else
		{ /* multi-process environment */ }

		try
		{
			data_->env_.open(path.c_str(), flags, 0);

			if (mode & transactions)
			{
				int return_value;
				switch (deadlock_policy)
				{
				case abort_default:
					return_value = data_->env_.set_lk_detect(DB_LOCK_DEFAULT);
					break;
				case abort_expire:
					return_value = data_->env_.set_lk_detect(DB_LOCK_EXPIRE);
					break;
				case abort_max_locks:
					return_value = data_->env_.set_lk_detect(DB_LOCK_MAXLOCKS);
					break;
				case abort_max_write_locks:
					return_value = data_->env_.set_lk_detect(DB_LOCK_MAXWRITE);
					break;
				case abort_min_locks:
					return_value = data_->env_.set_lk_detect(DB_LOCK_MINLOCKS);
					break;
				case abort_min_write_locks:
					return_value = data_->env_.set_lk_detect(DB_LOCK_MINWRITE);
					break;
				case abort_youngest:
					return_value = data_->env_.set_lk_detect(DB_LOCK_YOUNGEST);
					break;
				case abort_random:
					return_value = data_->env_.set_lk_detect(DB_LOCK_RANDOM);
					break;
				case abort_oldest:
					return_value = data_->env_.set_lk_detect(DB_LOCK_OLDEST);
					break;
				default: assert(false);
				}
			}
			else
			{ /* no need for deadlock detection */ }
		}
		catch (const DbException & e)
		{
			throw std::runtime_error(e.what());
		}
	}
			
	Environment::~Environment()
	{
		while (!databases_.empty())
		{
			Database * db(databases_.front());
			assert(db);
			db->close();
		}
		delete data_;
	}

	void Environment::setTransactionTimeout(unsigned microseconds)
	{
		data_->env_.set_timeout(microseconds, DB_SET_TXN_TIMEOUT);
	}

	void Environment::addDatabase(Database * db)
	{
		assert(db);
		assert("Registered database entry must be unique" &&
			std::find(databases_.begin(), databases_.end(), db) == databases_.end());
		databases_.push_back(db);
	}

	void Environment::removeDatabase(Database * db)
	{
		assert(db);
		Databases::iterator where(std::find(databases_.begin(), databases_.end(), db));
		assert("Database must be registered to remove it" &&
			where!= databases_.end());
		databases_.erase(where);
	}

	void Environment::checkpoint(unsigned flags/* = 0*/, unsigned min_kilobytes/* = 4*/, unsigned min_minutes/* = 1*/)
	{
		data_->env_.txn_checkpoint(flags, min_kilobytes, min_minutes);
	}

	void Environment::removeInactiveLogFiles()
	{
		data_->env_.log_archive(0, DB_ARCH_REMOVE);
	}
}


