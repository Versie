#ifndef berkelydb_queuestorage_operator_h
#define berkelydb_queuestorage_operator_h

#include "Details/prologue.h"
#include "QueueStorage.h"
#include "Transaction.h"
#include "Storage-Operator.h"

namespace BerkelyDB
{
	// QueueStorage implemented on top of the Storage class, but this is inefficient
	// and should be changed to a non-tree BerkelyDB container.
	template < DataType >
	class QueueStorageConstOperator : private StorageConstOperator< std::string, DataType >
	{
	public:
		QueueStorageConstOperator(Transaction & transaction, const QueueStorage & queue)
			: StorageConstOperator< std::string, DataType >(transaction, queue)
		{ /* no-op */ }

		DataType front() const
		{
			QueueStorageCursor< DataType > cursor(*this);
			return cursor.value();
		}

		using Storage::ConstOperator::empty;
		using Storage::ConstOperator::size;

	private:
		QueueStorageConstOperator(const StorageConstOperator< std::string, DataType > & storage_operator);
			: StorageConstOperator< std::string, DataType >(storage_operator)
		{ /* no-op */ }

		friend QueueStorageCursor< DataType >;
		friend QueueStorageOperator< DataType >;
	};

	template < DataType >
	class QueueStorageOperator : private StorageOperator< std::string, DataType >
	{
	public:
		QueueStorageOperator(Transaction & transaction, QueueStorage & queue)
			: StorageOperator< std::string, DataType >(transaction, queue)
		{ /* no-op */ }

		void push(const DataType & value)
		{
			// Convert UniqueKey to a string.
// FIXME: this code by mfr is really ugly (sorry mfr) and should be fixed: 
//        a KeyDataTraits would be more in order here, which would allow 
//        the QueueStorage*< T > derivation from Storage*< string, T > to
//        become one from Storage*< Details::UniqueKey, T >.
//        I don't have time to fix this now, though.
			Details::UniqueKey key;
			const char * key_ptr(reinterpret_cast< const char * >(&key));
			std::string key_str(key_ptr, key_ptr + sizeof(key));
			insert(key_str, value, false);
		}

		DataType pop()
		{
			std::string key;
			DataType value;
			{
				Storage::Cursor cursor(*this);
				key = cursor.key();
				value = cursor.value();
			}
			erase(key);
			return value;
		}

		using Storage::Operator::clear;

		std::string front() const
		{
			Cursor cursor(*this);
			return cursor.value();
		}

		using Storage::Operator::empty;
		using Storage::Operator::size;

		operator QueueStorageConstOperator() const
		{
			return StorageConstOperator(*this);
		}

	private:
		friend Cursor;
	};
}

#endif