#ifndef _versie_details_database_environment_h
#define _versie_details_database_environment_h

#include "Details/prologue.h"

#include <string>
#include <vector>

#include "Database.h"
#include "Transaction.h"

namespace BerkelyDB
{
	//! Class to wrap a handle to a database file
	class BERKELYDB_API Environment
	{
	public :
		//! Flags that can be used to open a database
		enum Flags {
			//! Create the environment if it doesn't exist yet
			create				= 1,
			//! Support transactions (mutually exclusive with writable cursors)
			transactions		= 2,
			//! Support writable cursors (mutually exclusive with transaction support)
			writable_cursors	= 4,
			//! Attempt database recovery on open (must have create and transaction flag set)
			recovery			= 8,
			//! Make the database environment capable of working from multiple concurent threads
			multithreaded		= 0x10,
			//! Optimize cache and locks assuming only one process is accessing the database
			single			= 0x20,
		};

		//! Determine what to do when encountering a deadlock.
		enum DeadlockPolicy {
			//! Use the lock policy specified when the database environment was created, or 
			//! abort_random if no policy has been specified
			abort_default,
			//! Abort only after lock requests have timed out
			abort_expire,
			//! Abort transaction having the most locks
			abort_max_locks,
			//! Abort transaction having the most write locks
			abort_max_write_locks,
			//! Abort transaction having the least locks
			abort_min_locks,
			//! Abort transaction having the least write locks
			abort_min_write_locks,
			//! Abort the oldest transaction
			abort_oldest,
			//! Abort a random transaction
			abort_random,
			//! Abort the youngest transaction
			abort_youngest,
		};
				
		//! Construct a handle from the name of the file
		/** As Berkely databases reside in files, this
		 * class opens the file with the Berkely back-end
		 * and will close it when destroyed. This enables
		 * you to use a Berkely database with RAII
		 *
		 * \todo we don't support environments yet */
		Environment(const std::string & path, int mode = create | writable_cursors, DeadlockPolicy deadlock_policy = abort_default);
		~Environment();

		void setTransactionTimeout(unsigned microseconds);

		int getMode() const { return mode_; }

		/** Flush logged operations (transactions) to the database file. If old log files aren't needed 
		 * (for catastrophic recovery for instance), a call to the removeInactiveLogFiles may be in order
		 * to free some disk space. */
		void checkpoint(unsigned flags = 0, unsigned min_kilobytes = 4, unsigned min_minutes = 1);
		/** Remove log files which are no longer active. */
		void removeInactiveLogFiles();

	private :
		// neither CopyConstructible nor Assignable
		Environment(const Environment &);
		Environment & operator=(const Environment &);

		struct Data;
		typedef std::vector< Database * > Databases;

		void addDatabase(Database * db);
		void removeDatabase(Database * db);

		Data * data_;
		std::string path_;
		Databases databases_;
		int mode_;

		friend class Database;
		friend struct Database::Data;
		friend struct Transaction::Data;
		friend struct Database::DatabaseAdditionGuard;
	};
}

#endif

