struct Cursor::Data
{
	Data()
		: cursor_(0),
		  key_value_cached_(false),
		  data_value_cached_(false)
	{ /* no-op */ }

	Data(const Data & data)
		: cursor_(0),
		  key_value_cached_(false),
		  data_value_cached_(false)
	{
		if (data.cursor_)
			if (data.cursor_->dup(&cursor_, DB_POSITION) != 0)
				throw Exceptions::CursorError(Exceptions::CursorError::duplication_failed);
			else
			{ /* duplication OK */ }
		else
		{ /* nothing to do */ }
		assert(cursor_);
	}

	~Data()
	{
		if (cursor_)
			cursor_->close();
		else
		{ /* cursor not set */ }
	}
	
	Dbc * cursor_;
	bool key_value_cached_;
	std::vector< char > key_value_;
	bool data_value_cached_;
	std::vector< char > data_value_;
};
