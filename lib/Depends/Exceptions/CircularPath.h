#ifndef _depends_exceptions_circularpath_h
#define _depends_exceptions_circularpath_h

#include <stdexcept>

namespace Depends
{
	namespace Exceptions
	{
		struct CircularPath : std::runtime_error
		{
			CircularPath()
				: std::runtime_error("Circular path in Directed Acyclic Graph")
			{ /* no-op */ }
		};
	}
}

#endif
