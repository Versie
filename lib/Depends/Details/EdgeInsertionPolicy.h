#ifndef _depends_details_edgeinsertionpolicy_h
#define _depends_details_edgeinsertionpolicy_h

#include <stack>
#include <utility>
#include <boost/tuple/tuple.hpp>
#include <boost/utility/enable_if.hpp>
#include "Exceptions/CircularPath.h"
#include <Visitors/visit.h>
#include <Visitors/Policies/DepthFirstTraversalPolicy.h>

namespace Depends
{
	namespace Details
	{
		/*
		 * The way edges are inserted in the graph can change radically, depending
		 * on whether the edge type exists and whether the graph is acyclic. If the
		 * edge type doesn't exist, the vertex needs to be updated itself whereas
		 * if it does exist, the vertex only needs to be updated for acyclic graphs.
		 */

		// this version is used when the edge exists and the graph is not acyclic - i.e. it is the default implementation
		template < typename GraphType, typename VertexDataType, typename EdgeDataType, bool acyclic__, bool require_bidirectional_traversal__, typename Enable = void >
		struct EdgeInsertionPolicy
		{
		private :
			typedef typename GraphType::edge_iterator EdgeIteratorType_;
			typedef typename GraphType::Edge EdgeType_;
			typedef typename GraphType::vertex_iterator VertexIteratorType_;

		public:
			static EdgeIteratorType_ insert(GraphType * this_, VertexIteratorType_ source, VertexIteratorType_ target, const EdgeDataType & edge_data)
			{
				typename GraphType::Edges_ & edges(this_->edges_);
				EdgeType_ edge(edge_data);
				edge.source_iter_ = source;
				edge.target_iter_ = target;
				edge.source_ = *source;
				edge.target_ = *target;

				return edges.insert(edge).first;
			}
		};

		// this version is used when we don't have an edge type and the graph is not acyclic
		template < typename GraphType, typename VertexDataType, typename EdgeDataType, bool acyclic__, bool require_bidirectional_traversal__ >
		struct EdgeInsertionPolicy<
			GraphType,
			VertexDataType,
			EdgeDataType,
			acyclic__,
			require_bidirectional_traversal__,
			typename boost::enable_if<
				typename boost::mpl::and_<
					typename edge_type_is_null< EdgeDataType, require_bidirectional_traversal__ >::type,
					typename boost::mpl::not_< typename boost::mpl::bool_< acyclic__ >::type >
				>::type
			>::type
		>
		{
		private :
			typedef typename GraphType::edge_iterator EdgeIteratorType_;
			typedef typename GraphType::vertex_iterator VertexIteratorType_;

		public:
			static EdgeIteratorType_ insert(GraphType * this_, VertexIteratorType_ source, VertexIteratorType_ target, const NullType &)
			{
				source->targets_.push_back(&(*target));
				return EdgeIteratorType_();
			}
		};

		// this version is used when we do have an edge type and the graph is acyclic
		template < typename GraphType, typename VertexDataType, typename EdgeDataType, bool acyclic__, bool require_bidirectional_traversal__ >
		struct EdgeInsertionPolicy< 
			GraphType,
			VertexDataType,
			EdgeDataType,
			acyclic__,
			require_bidirectional_traversal__,
			typename boost::enable_if<
				typename boost::mpl::and_<
					typename boost::mpl::not_< typename edge_type_is_null< EdgeDataType, require_bidirectional_traversal__ >::type >::type,
					typename boost::mpl::bool_< acyclic__ >::type 
				>::type 
			>::type 
		>
		{
		private :
			typedef typename GraphType::edge_iterator EdgeIteratorType_;
			typedef typename GraphType::Edge EdgeType_;
			typedef typename GraphType::vertex_iterator VertexIteratorType_;

			struct Adapter_;
			struct Visitor_
			{
				typedef Visitors::Policies::DepthFirstTraversalPolicy< Adapter_ > traversal_policy_type;
				typedef int Visitor_::* Bool_;

				Visitor_(const VertexDataType & to_find, bool & found)
					: to_find_(to_find),
					  found_(found)
				{ /* no-op */ }

				const Visitor_ & operator()(const Adapter_ & current) const
				{
					found_ |= (current->get() == to_find_);
					return *this;
				}

				operator Bool_ () const
				{
					return found_ ? &Visitor_::used_for_cast_to_bool_ : 0;
				}

			private :
				const VertexDataType & to_find_;
				bool & found_;
				int used_for_cast_to_bool_;
			};

			struct Adapter_
			{
				typedef int Adapter_::* Bool_;
				typedef std::stack< std::pair< VertexIteratorType_, unsigned long > > Parents;

				Adapter_(GraphType * graph, bool null, VertexIteratorType_ top, Parents parents, VertexIteratorType_ current, unsigned long sibling_index)
					: graph_(graph),
					  null_(null),
					  top_(top),
					  parents_(parents),
					  current_(current),
					  sibling_index_(sibling_index)
				{ /* no-op */ }

				Adapter_ getParent() 
				{
					if (!parents_.empty())
					{
						Parents parents(parents_);
						VertexIteratorType_ new_curr;
						unsigned long new_curr_index;
						boost::tie(new_curr, new_curr_index) = parents.top();
						parents.pop();
						return Adapter_(graph_, false, top_, parents_, new_curr, new_curr_index);
					}
					else
						return Adapter_(graph_, true, top_, parents_, current_, 0);
				};

				Adapter_ getNextSibling() 
				{
					Adapter_ parent(getParent());
					if (!parent)
						return parent;
					else
					{ /* carry on */ }
					if (parent.current_->targets_.size() > sibling_index_ + 1)
					{
						VertexIteratorType_ new_curr(graph_->vertices_.find(*(parent.current_->targets_[sibling_index_ + 1])));
						return Adapter_(graph_, false, top_, parents_, new_curr, sibling_index_ + 1);
					}
					else
						return Adapter_(graph_, true, top_, parents_, current_, 0);
				};

				Adapter_ getFirstChild() 
				{
					if (current_->targets_.empty())
						return Adapter_(graph_, true, top_, parents_, current_, 0);
					else
					{ /* carry on */ }
					VertexIteratorType_ new_curr(graph_->vertices_.find(*(current_->targets_[0])));
					Parents parents(parents_);
					parents.push(std::make_pair(current_, sibling_index_));
					return Adapter_(graph_, false, top_, parents, new_curr, 0);
				};

				operator Bool_ () const
				{
					return null_ ? 0 : &Adapter_::used_for_cast_to_bool_;
				}

				VertexIteratorType_ operator->() const
				{
					return current_;
				}

				GraphType * graph_;
				bool null_;
				VertexIteratorType_ top_;
				Parents parents_;
				VertexIteratorType_ current_;
				unsigned long sibling_index_;
				int used_for_cast_to_bool_;
			};

		public:
			static EdgeIteratorType_ insert(GraphType * this_, VertexIteratorType_ source, VertexIteratorType_ target, const EdgeDataType & edge_data)
			{
				// with a visitor, check that a link in the opposite direction does not already exist
				bool found(false);
				Visitor_ visitor(source->get(), found);
				Visitors::visit(visitor, Adapter_(this_, false, target, Adapter_::Parents(), target, 0));
				if (found)
					throw Exceptions::CircularPath();
				else
				{ /* go ahead - the new path won't be circular */ }
				source->targets_.push_back(&(*target));

				typename GraphType::Edges_ & edges(this_->edges_);
				EdgeType_ edge(edge_data);
				edge.source_iter_ = source;
				edge.target_iter_ = target;
				edge.source_ = *source;
				edge.target_ = *target;

				return edges.insert(edge).first;
			}
		};

		// this version is used when we don't have an edge type and the graph is acyclic
		template < typename GraphType, typename VertexDataType, typename EdgeDataType, bool acyclic__, bool require_bidirectional_traversal__ >
		struct EdgeInsertionPolicy< 
			GraphType,
			VertexDataType,
			EdgeDataType,
			acyclic__,
			require_bidirectional_traversal__,
			typename boost::enable_if<
				typename boost::mpl::and_<
				typename edge_type_is_null< EdgeDataType, require_bidirectional_traversal__ >::type,
				typename boost::mpl::bool_< acyclic__ >::type 
				>::type 
			>::type
		>
		{
		private :
			typedef typename GraphType::edge_iterator EdgeIteratorType_;
			typedef typename GraphType::vertex_iterator VertexIteratorType_;

			struct Adapter_;
			struct Visitor_
			{
				typedef Visitors::Policies::DepthFirstTraversalPolicy< Adapter_ > traversal_policy_type;
				typedef int Visitor_::* Bool_;

				Visitor_(const VertexDataType & to_find, bool & found)
					: to_find_(to_find),
					found_(found)
				{ /* no-op */ }

				const Visitor_ & operator()(const Adapter_ & current) const
				{
					found_ |= (current->get() == to_find_);
					return *this;
				}

				operator Bool_ () const
				{
					return found_ ? &Visitor_::used_for_cast_to_bool_ : 0;
				}

			private :
				const VertexDataType & to_find_;
				bool & found_;
				int used_for_cast_to_bool_;
			};

			struct Adapter_
			{
				typedef int Adapter_::* Bool_;
				typedef std::stack< std::pair< VertexIteratorType_, unsigned long > > Parents;

				Adapter_(GraphType * graph, bool null, VertexIteratorType_ top, Parents parents, VertexIteratorType_ current, unsigned long sibling_index)
					: graph_(graph),
					null_(null),
					top_(top),
					parents_(parents),
					current_(current),
					sibling_index_(sibling_index)
				{ /* no-op */ }

				Adapter_ getParent() 
				{
					if (!parents_.empty())
					{
						Parents parents(parents_);
						VertexIteratorType_ new_curr;
						unsigned long new_curr_index;
						boost::tie(new_curr, new_curr_index) = parents.top();
						parents.pop();
						return Adapter_(graph_, false, top_, parents_, new_curr, new_curr_index);
					}
					else
						return Adapter_(graph_, true, top_, parents_, current_, 0);
				};

				Adapter_ getNextSibling() 
				{
					Adapter_ parent(getParent());
					if (!parent)
						return parent;
					else
					{ /* carry on */ }
					if (parent.current_->targets_.size() > sibling_index_ + 1)
					{
						VertexIteratorType_ new_curr(graph_->vertices_.find(*(parent.current_->targets_[sibling_index_ + 1])));
						return Adapter_(graph_, false, top_, parents_, new_curr, sibling_index_ + 1);
					}
					else
						return Adapter_(graph_, true, top_, parents_, current_, 0);
				};

				Adapter_ getFirstChild() 
				{
					if (current_->targets_.empty())
						return Adapter_(graph_, true, top_, parents_, current_, 0);
					else
					{ /* carry on */ }
					VertexIteratorType_ new_curr(graph_->vertices_.find(*(current_->targets_[0])));
					Parents parents(parents_);
					parents.push(std::make_pair(current_, sibling_index_));
					return Adapter_(graph_, false, top_, parents, new_curr, 0);
				};

				operator Bool_ () const
				{
					return null_ ? 0 : &Adapter_::used_for_cast_to_bool_;
				}

				VertexIteratorType_ operator->() const
				{
					return current_;
				}

				GraphType * graph_;
				bool null_;
				VertexIteratorType_ top_;
				Parents parents_;
				VertexIteratorType_ current_;
				unsigned long sibling_index_;
				int used_for_cast_to_bool_;
			};

		public:
			static EdgeIteratorType_ insert(GraphType * this_, VertexIteratorType_ source, VertexIteratorType_ target, const NullType &)
			{
				// with a visitor, check that a link in the opposite direction does not already exist
				bool found(false);
				Visitor_ visitor(source->get(), found);
				Visitors::visit(visitor, Adapter_(this_, false, target, Adapter_::Parents(), target, 0));
				if (found)
					throw Exceptions::CircularPath();
				else
				{ /* go ahead - the new path won't be circular */ }
				source->targets_.push_back(&(*target));
				return EdgeIteratorType_();
			}
		};
	}
}

#endif
