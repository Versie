#ifndef _depends_details_edge_h
#define _depends_details_edge_h

namespace Depends
{
	namespace Details
	{
		/* Type of an edge. If it exists (which depends on whether or not the edge needs 
		 * to carry any data) it contains two iterators and contains the corresponding 
		 * values. This latter detail is a convenience that comes at the price of the 
		 * storage needed to hold two values, multiplied by the number of edges. It is,
		 * however, necessary for the requirement that the edge (or whatever is returned by
		 * findEdges) contains a member called "source_" and a member called "target" 
		 * which is convertible to the vertex data-type. 
		 * It should be possible to use a proxy object for this in stead, at least
		 * for types that would be larger than that proxy object. (FIXME) */
		template < typename EdgeDataType, typename VertexIteratorType >
		struct Edge
		{
			Edge()
			{ /* no-op */ }

			Edge(EdgeDataType data)
				: value_(data)
			{ /* no-op */ }

			bool operator<(const Edge & rhs) const
			{
				return value_ < rhs.value_;
			}

			bool operator==(const Edge & rhs) const
			{
				return value_ == rhs.value_;
			}

			EdgeDataType value_;
			VertexIteratorType source_iter_;
			VertexIteratorType target_iter_;
			typename std::iterator_traits< VertexIteratorType >::value_type source_;
			typename std::iterator_traits< VertexIteratorType >::value_type target_;
		};
	}
}

#endif
