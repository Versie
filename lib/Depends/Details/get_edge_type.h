#ifndef _depends_details_get_edge_type_h
#define _depends_details_get_edge_type_h

#include <boost/mpl/identity.hpp>
#include "get_vertex_iterator_type.h"

namespace Depends
{
	namespace Details
	{
		template < typename EdgeDataType, typename VertexType >
		struct Edge;
		struct NullType;

		/*
		 * The edge type is a function of the type of data it will have to 
		 * carry, whether or not it should always exist (if 
		 * require_bidirectional_traversal__ is false, it doens't need to 
		 * exist if it doesn't carry any data) and the type of the vertices.
		 */
		template < typename GraphType, typename VertexDataType, typename EdgeDataType, bool require_bidirectional_traversal__ >
		struct get_edge_type : 
			boost::mpl::identity< 
				Edge< 
					EdgeDataType, 
					typename get_vertex_iterator_type< GraphType, VertexDataType, EdgeDataType, require_bidirectional_traversal__ >::type > >
		{};

		template < typename GraphType, typename VertexDataType >
		struct get_edge_type< GraphType, VertexDataType, NullType, false > : boost::mpl::identity< NullType >
		{};
	}
}

#endif
