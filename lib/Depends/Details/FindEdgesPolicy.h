#ifndef _depends_details_findedgespolicy_h
#define _depends_details_findedgespolicy_h

#include <algorithm>
#include <boost/tuple/tuple.hpp>
#include "NullType.h"
#include "get_edge_type.h"
#include "get_vertex_type.h"
#include "Vertex.h"

namespace Depends
{
	namespace Details
	{
		template < typename GraphType, typename VertexDataType, typename EdgeDataType, bool acyclic__, bool require_bidirectional_traversal__, typename Enable = void >
		struct FindEdgesPolicy
		{
		private :
			typedef typename get_vertex_type< VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >::type Vertex_;

		public :
			typedef typename get_edge_type< GraphType, VertexDataType, EdgeDataType, require_bidirectional_traversal__ >::type Edge;
			typedef Edge Edge_;

			/* The Edges structure is implementation-defined in that all 
			 * we know is that it provides a begin() and end() method, both
			 * of which provide random-access iterators into the same 
			 * sequence. */
			typedef std::vector< Edge > Edges;

			static Edges findEdges(const GraphType * graph, const Vertex_ & vertex)
			{
				Edges retval;
				const typename GraphType::Edges_ & edges(graph->edges_);
				typename GraphType::Edges_::const_iterator end(edges.end());
				for (typename GraphType::Edges_::const_iterator curr(edges.begin()); curr != end; ++curr)
				{
					if ((curr->source_) == vertex)
						retval.push_back(*curr);
					else if ((curr->target_) == vertex)
						retval.push_back(*curr);
					else 
					{ /* this edge does not refer to the vertex in question, so nothing to do here. */ }
				}

				return retval;
			}
		};

		/*
		 * If edges don't carry any data and we're not required to allow 
		 * bidirectional traversal, edges are abstracted as pointers
		 */
		template < typename GraphType, typename VertexDataType, typename EdgeDataType, bool acyclic__, bool require_bidirectional_traversal__ >
		struct FindEdgesPolicy< GraphType, VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__, typename boost::enable_if< typename edge_type_is_null< EdgeDataType, require_bidirectional_traversal__ >::type >::type > : NullType
		{
			typedef typename get_vertex_type< VertexDataType, NullType, false, false >::type Vertex_;
			struct Edge_
			{
				Edge_(const VertexDataType & source, const VertexDataType & target)
					: source_(source),
					  target_(target)
				{ /* no-op */ }

				NullType value_;
				VertexDataType source_;
				VertexDataType target_;
			};

			/* The Edges structure is implementation-defined in that all 
			 * we know is that it provides a begin() and end() method, both
			 * of which provide random-access iterators into the same 
			 * sequence. */
			typedef std::vector< Edge_ > Edges;

			struct Visitor_
			{
				Visitor_(const VertexDataType & data, Edges & edges)
					: data_(data),
					  edges_(edges)
				{ /* no-op */ }

				const Visitor_ & operator()(const Vertex_ & vertex) const
				{
					if (vertex.get() == data_)
					{
						// copy all the targets of this vertex into the edges set
						typename Vertex_::Targets_::const_iterator end(vertex.targets_.end());
						for (typename Vertex_::Targets_::const_iterator curr(vertex.targets_.begin()); curr != end; ++curr)
						{
							Edge_ edge(vertex.get(), (*curr)->get());
							edges_.push_back(edge);
						}
					}
					else
					{
						// scan all of the targets of this vertex to see if the one we're looking for is in there
						// copy all the targets of this vertex into the edges set
						typename Vertex_::Targets_::const_iterator end(vertex.targets_.end());
						for (typename Vertex_::Targets_::const_iterator curr(vertex.targets_.begin()); curr != end; ++curr)
						{
							if ((*curr)->get() == data_)
							{
								Edge_ edge(vertex.get(), (*curr)->get());
								edges_.push_back(edge);
							}
							else
							{ /* skip this one */ }
						}
					}

					return *this;
				}

				const VertexDataType & data_;
				Edges & edges_;
			};

			typedef typename get_edge_type< GraphType, VertexDataType, NullType, false >::type Edge;

			static Edges findEdges(const GraphType * graph, const Vertex_ & vertex)
			{
				Edges retval;
				const typename GraphType::Vertices_ & vertices(graph->vertices_);
				std::for_each(vertices.begin(), vertices.end(), Visitor_(vertex.get(), retval));

				return retval;
			}

			// find all edges in which the given vertex is implicated
			static Edges findEdges(const GraphType * graph, const VertexDataType & _vertex)
			{
				bool found(false);
				Vertex_ vertex;
				boost::tie(found, vertex) = graph->findVertex(_vertex);

				if (found)
					return findEdges(graph, vertex);
				else
				{
					// if the vertex wasn't found, return an empty result set
					return Edges();
				}
			}
		};
	}
}

#endif
