#ifndef _depends_details_get_const_edge_iterator_type_h
#define _depends_details_get_const_edge_iterator_type_h

#include <boost/mpl/identity.hpp>
#include <boost/mpl/if.hpp>
#include <boost/type_traits/is_same.hpp>

namespace Depends
{
	namespace Details
	{
		template < typename GraphType, typename VertexDataType, typename EdgeDataType, bool require_bidirectional_traversal__, typename Enable = void >
		struct get_const_edge_iterator_type : boost::mpl::identity< typename GraphType::Edges_::const_iterator >
		{};

		template < typename GraphType, typename VertexDataType, typename EdgeDataType, bool require_bidirectional_traversal__ >
		struct get_const_edge_iterator_type<
			GraphType, VertexDataType,
			EdgeDataType,
			require_bidirectional_traversal__,
			typename boost::enable_if< 
				typename boost::is_same<
					typename get_edge_type< GraphType, VertexDataType, EdgeDataType, require_bidirectional_traversal__ >::type,
					Details::NullType
				>::type
			>::type
		> : boost::mpl::identity< NullType >
		{};
	}
}

#endif
