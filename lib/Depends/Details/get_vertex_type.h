#ifndef _depends_details_get_vertex_type_h
#define _depends_details_get_vertex_type_h

#include <boost/mpl/identity.hpp>
#include <boost/mpl/logical.hpp>
#include <boost/utility/enable_if.hpp>
#include "edge_type_is_null.h"
#include "Vertex.h"

namespace Depends
{
	namespace Details
	{
		struct NullType;

		/*
		 * The vertex type is a function of the data type it will carry 
		 * and whether or not the edge type exists. If it does exist, the
		 * vertex doesn't need to carry any information about the graph 
		 * itself an can therefore be represented by the data it carries.
		 * If it doesn't exist, the vertex will need a vector of pointers
		 * to its targets.
		 */
		template < typename VertexDataType, typename EdgeDataType, bool acyclic__, bool require_bidirectional_traversal__, typename Enable = void >
		struct get_vertex_type : boost::mpl::identity< Vertex< VertexDataType > >
		{};
		template < typename VertexDataType, typename EdgeDataType, bool acyclic__, bool require_bidirectional_traversal__ >
		struct get_vertex_type<
			VertexDataType,
			EdgeDataType,
			acyclic__,
			require_bidirectional_traversal__,
			typename boost::enable_if<
				typename boost::mpl::and_<
					typename edge_type_is_null< EdgeDataType, require_bidirectional_traversal__ >::type,
					typename boost::mpl::not_< typename boost::mpl::bool_< acyclic__ >::type >::type
				>::type
			>
		> : boost::mpl::identity< VertexDataType >
		{};
	}
}

#endif
