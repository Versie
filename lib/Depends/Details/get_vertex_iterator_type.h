#ifndef _depends_details_get_vertex_iterator_type_h
#define _depends_details_get_vertex_iterator_type_h

#include <boost/mpl/identity.hpp>

namespace Depends
{
	namespace Details
	{
		template < typename GraphType, typename VertexDataType, typename EdgeDataType, bool require_bidirectional_traversal__ >
		struct get_vertex_iterator_type
		{
			typedef typename GraphType::Vertices_ intermediate_;
			typedef typename intermediate_::iterator type;
		};
	}
}

#endif
