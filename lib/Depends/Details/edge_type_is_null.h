#ifndef _depends_details_edge_type_is_null_h
#define _depends_details_edge_type_is_null_h

#include <boost/type_traits/integral_constant.hpp>

namespace Depends
{
	namespace Details
	{
		struct NullType;

		/*
		 * The edge type is null if the edge does not carry any data and 
		 * bidirectional traversal is not necessary.
		 */

		template < typename EdgeDataType, bool require_bidirectional_traversal__ >
		struct edge_type_is_null : boost::false_type
		{};
		template <>
		struct edge_type_is_null< NullType, false > : boost::true_type
		{};
	}
}

#endif
