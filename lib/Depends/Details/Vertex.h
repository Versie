#ifndef _depends_details_vertex_h
#define _depends_details_vertex_h

#include <vector>
#include <boost/type_traits/is_class.hpp>

namespace Depends
{
	namespace Details
	{
		template < typename VertexDataType, typename Enable = void >
		struct Vertex : VertexDataType
		{
			typedef VertexDataType DataType_;
			typedef std::vector< const Vertex< VertexDataType >* > Targets_;

			Vertex()
			{ /* no-op */ }

			Vertex(const VertexDataType & data)
				: VertexDataType(data)
			{ /* no-op */ }

			const VertexDataType & get() const
			{
				return *this;
			}

			VertexDataType & get()
			{
				return *this;
			}

			mutable Targets_ targets_;
		};

		template < typename VertexDataType >
		struct Vertex< VertexDataType, typename boost::enable_if< typename boost::mpl::not_< typename boost::is_class< VertexDataType >::type >::type >::type >
		{
			typedef VertexDataType DataType_;
			typedef std::vector< const Vertex< VertexDataType >* > Targets_;

			Vertex()
			{ /* no-op */ }

			Vertex(const VertexDataType & data)
				: data_(data)
			{ /* no-op */ }

			bool operator<(const Vertex & rhs) const
			{
				return data_ < rhs.data_;
			}
			bool operator==(const Vertex & rhs) const
			{
				return data_ == rhs.data_;
			}

			const VertexDataType & get() const
			{
				return data_;
			}

			VertexDataType & get()
			{
				return data_;
			}

			VertexDataType data_;
			mutable Targets_ targets_;
		};
	}
}

#endif
