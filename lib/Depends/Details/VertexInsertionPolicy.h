#ifndef _depends_details_vertexinsertionpolicy_h
#define _depends_details_vertexinsertionpolicy_h

namespace Depends
{
	namespace Details
	{
		template < typename GraphType, typename VertexDataType, typename EdgeDataType, bool require_bidirectional_traversal__ >
		struct VertexInsertionPolicy
		{
		private :
			typedef typename GraphType::vertex_iterator VertexIteratorType_;

		public:
			static VertexIteratorType_ insert(GraphType * this_, const VertexDataType & data)
			{
				typename GraphType::Vertices_ & vertices(this_->vertices_);

				return vertices.insert(data).first;
			}
		};
	}
}

#endif
