#ifndef _depends_details_findverticespolicy_h
#define _depends_details_findverticespolicy_h

#include "get_vertex_type.h"

namespace Depends
{
	namespace Details
	{
		template < typename GraphType, typename VertexDataType, typename EdgeDataType, bool acyclic__, bool require_bidirectional_traversal__ >
		struct FindVerticesPolicy
		{
		public :
			typedef typename get_vertex_type< VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >::type Vertex;
			static std::pair< bool /* found */, Vertex > findVertex(const GraphType * graph, const VertexDataType & vertex)
			{
				const typename GraphType::Vertices_ & vertices(graph->vertices_);
				typename GraphType::Vertices_::const_iterator end(vertices.end());
				typename GraphType::Vertices_::const_iterator where(vertices.find(Vertex(vertex)));
				if (where != end)
					return std::make_pair(true, *where);
				else
					return std::make_pair(false, Vertex());
			}
		};
	}
}

#endif
