#ifndef _depends_directedgraph_h
#define _depends_directedgraph_h

#include <set>
#include <boost/mpl/if.hpp>
#include <boost/type_traits/is_same.hpp>
#include "Details/Edge.h"
#include "Details/NullType.h"
#include "Details/FindEdgesPolicy.h"
#include "Details/FindVerticesPolicy.h"
#include "Details/get_const_edge_iterator_type.h"
#include "Details/get_edge_iterator_type.h"
#include "Details/get_const_vertex_iterator_type.h"
#include "Details/get_vertex_iterator_type.h"
#include "Details/EdgeInsertionPolicy.h"
#include "Details/VertexInsertionPolicy.h"

namespace Depends
{
	template < typename VertexDataType, typename EdgeDataType, bool acyclic__ = false, bool require_bidirectional_traversal__ = true >
	class DirectedGraph
	{
	public :
		typedef typename Details::get_vertex_type< VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >::type Vertex_;
		typedef std::set< Vertex_ > Vertices_;
		typedef Details::FindVerticesPolicy< DirectedGraph< VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >, VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ > FindVerticesPolicy_;
		typedef Details::FindEdgesPolicy< DirectedGraph< VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >, VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ > FindEdgesPolicy_;
		typedef std::vector< typename FindEdgesPolicy_::Edge_ > Edges;
		typedef typename FindEdgesPolicy_::Edge Edge;
		typedef typename boost::mpl::if_< boost::is_same< Edge, Details::NullType >, Details::NullType, std::set< Edge > >::type Edges_;
		typedef typename Details::get_vertex_iterator_type< DirectedGraph< VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >, VertexDataType, EdgeDataType, require_bidirectional_traversal__ >::type vertex_iterator;
		typedef typename Details::get_const_vertex_iterator_type< DirectedGraph< VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >, VertexDataType, EdgeDataType, require_bidirectional_traversal__ >::type const_vertex_iterator;
		typedef typename Details::get_edge_iterator_type< DirectedGraph< VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >, VertexDataType, EdgeDataType, require_bidirectional_traversal__ >::type edge_iterator;
		typedef typename Details::get_const_edge_iterator_type< DirectedGraph< VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >, VertexDataType, EdgeDataType, require_bidirectional_traversal__ >::type const_edge_iterator;

		typename FindEdgesPolicy_::Edges findEdges(const VertexDataType & _vertex) const
		{
			return FindEdgesPolicy_::findEdges(this, _vertex);
		}

		std::pair< bool /* found */, Vertex_ > findVertex(const VertexDataType & vertex) const
		{
			return FindVerticesPolicy_::findVertex(this, vertex);
		}

		edge_iterator insert(vertex_iterator source, vertex_iterator target, const EdgeDataType & edge_data)
		{
			return EdgeInsertionPolicy_::insert(this, source, target, edge_data);
		}

		edge_iterator insert(
			const std::pair< VertexDataType, VertexDataType > & source_target_pair,
			const EdgeDataType & edge_data)
		{
			return insert(VertexInsertionPolicy_::insert(this, source_target_pair.first), VertexInsertionPolicy_::insert(this, source_target_pair.second), edge_data);
		}

	private :
		typedef Details::VertexInsertionPolicy< DirectedGraph< VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >, VertexDataType, EdgeDataType, require_bidirectional_traversal__ > VertexInsertionPolicy_;
		typedef Details::EdgeInsertionPolicy< DirectedGraph< VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >, VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ > EdgeInsertionPolicy_;

		Vertices_ vertices_;
		Edges_ edges_;

		friend struct Details::FindEdgesPolicy< DirectedGraph< VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >, VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >;
		friend struct Details::FindVerticesPolicy< DirectedGraph< VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >, VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >;
		friend struct Details::EdgeInsertionPolicy< DirectedGraph< VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >, VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >;
		friend struct Details::VertexInsertionPolicy< DirectedGraph< VertexDataType, EdgeDataType, acyclic__, require_bidirectional_traversal__ >, VertexDataType, EdgeDataType, require_bidirectional_traversal__ >;
	};

	template < typename VertexDataType, bool acyclic__, bool require_bidirectional_traversal__ >
	class DirectedGraph< VertexDataType, void, acyclic__, require_bidirectional_traversal__ > : public DirectedGraph< VertexDataType, Details::NullType, acyclic__, require_bidirectional_traversal__ >
	{
	public :
		void insert(const std::pair< VertexDataType, VertexDataType > & source_target_pair)
		{
			DirectedGraph< VertexDataType, Details::NullType, acyclic__, require_bidirectional_traversal__ >::insert(source_target_pair, Details::NullType());
		}
	};
}

#endif
