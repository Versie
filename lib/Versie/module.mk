# the library
Versie_SRC =	Application.cpp \
		Dependency.cpp \
		Release.cpp \
		Version.cpp

Versie_INSTALL_HEADERS += $(patsubst %.cpp,%.h,$(Versie_SRC))
SRC += $(patsubst %,lib/Versie/%,$(Versie_SRC))

INSTALL_HEADERS += $(patsubst %,lib/Versie/%,$(Versie_INSTALL_HEADERS))
Versie_OBJ := $(patsubst %.cpp,lib/Versie/%.lo,$(Versie_SRC))
OBJ += $(Versie_OBJ)

$(eval $(call LINK_LIBRARY_template,libVersie.la,$(Versie_OBJ)))
