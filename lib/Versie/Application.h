#ifndef _versie_application_h
#define _versie_application_h

#include <string>

namespace Versie
{
	class Application
	{
	public :
	private :
		std::string name_;
		std::string description_;
		Release release_;
		Version version_;
		std::vector< Dependency > dependencies_;
	};
}

#endif

