#ifndef _versie_versie_h
#define _versie_versie_h

namespace BerkelyDB
{
	class Environment;
}
namespace Versie
{
	/** This structure holds the database environment the other classes
	 * will use to get their databases. As such, it should be
	 * instantiated (using the getInstance method) before all others and
	 * its database environment should be set at that time. If that is
	 * not done, the application will fail on the first attempt to get
	 * any data. */
	class Versie
	{
	public :
		static Versie & getInstance();

		BerkelyDB::Environment * getDBEnvironment() const { return db_environment_; }
		void setDBEnvironment(BerkelyDB::Environment * db_environment) { db_environment_ = db_environment; }

	private :
		Versie() : db_environment_(0) {}

		BerkelyDB::Environment * db_environment_;
	};
}

#endif

