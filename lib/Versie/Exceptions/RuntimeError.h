#ifndef _versie_exceptions_runtimeerror_h
#define _versie_exceptions_runtimeerror_h

namespace Versie
{
	namespace Exceptions
	{
		class RuntimeError
		{
		public :
			enum What { database_environment_not_set };

			RuntimeError(What code)
				: what_(code)
			{ /* no-op */ }

			const char * what() const throw()
			{
				switch (what_)
				{
				case database_environment_not_set :
					return "Database environment not set";
				default :
					return "Unknown internal error";
				}
			}

		private :
			What what_;
		};
	}
}

#endif

