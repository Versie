#include "Versie.h"
#include <cassert>

namespace Versie
{
	static Versie & Versie::getInstance()
	{
		static Versie * instance__(0);

		if (!instance__)
			instance__ = new Versie;
		else
		{ /* already have an instance */ }
		assert(instance__);
		return *instance__;
	}
}


