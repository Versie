#ifndef versie_repository_h
#define versie_repository_h

#include <boost/noncopyable.hpp>
#include "Packages.h"

namespace Versie
{
	class Repository : boost::noncopyable
	{
	public :
		Repository(const std::string & address);
		Repository(const boost::filesystem::path & location);
		~Repository();

		bool isLocal() const;

		Packages getPackages() const;

		void insert(const Package & package);
	};
	void push(const Repository & from_repository /* must be local */, Repository & to_repository);
	void pull(const Repository & from_repository, Repository & to_repository /* must be local */);
}

#endif
