#include "Applications.h"
#include <cassert>
#include "Versie.h"

namespace Versie
{
	Applications & Applications::getInstance()
	{
		Applications * Applications::instance__(0);

		if (!instance__)
			instance__ = new Applications;
		else
		{ /* already have an instance */ }
		assert(instance__);
		return *instance__;
	}

	Applications::Applications()
		: database_(0)
	{
		Versie & config(Versie::getInstance());
		BerkelyDB::Environment * env(config.getDBEnvironment());
		if (!env)
			throw Exceptions::RuntimeError(Exceptions::RuntimeError::database_environment_not_set);
		else
		{ /* all is well - open the database */ }
		// note that this pointer is leaked but, due to the use of
		// the environment which belongs to the client app, the
		// database resources are not: as it is up to the client to
		// destroy the environment and because destroying the environment
		// effectively cleans up the database, no resources are leaked
		// (and memory leaks can easily be handled by the OS)
		database_ = new BerkelyDB::Database(env, "applications.db");
	}
}

