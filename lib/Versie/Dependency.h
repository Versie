#ifndef _versie_dependency_h
#define _versie_dependency_h

namespace Versie
{
	//! Class to model a dependency from one application to another.
	/** In order to know which version of an application or library
	 * some other application depends on, we need to know the name,
	 * type (application or library) and version (in terms of compatibility
	 * version) of the dependency. */
	class Dependency
	{
	public :
		enum Type { application, library }
	private :
		std::string dependent_name_;
		Version dependent_version_;
	};
}

#endif

