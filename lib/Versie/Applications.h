#ifndef _versie_applications_h
#define _versie_applications_h

#include <vector>

namespace BerkelyDB
{
	class Database;
}
namespace Versie
{
	class Application;
	
	class Applications
	{
	public :
		typedef std::vector< Application * > Applications_;
		typedef Applications_::const_iterator Iterator;

		static Applications & getInstance();

		Iterator begin() const;
		Iterator end() const;

	private :
		Applications();
		~Applications();

		BerkelyDB::Database * database_;
		Applications_ applications_;
	};
}

#endif

